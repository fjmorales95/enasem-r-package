library(tidyverse)

a_01 <- read_raw_data("../../Datos/2001/sect_a.dta")

a_03 <- read_raw_data("../../Datos/2003/sect_a.dta")

data_12 <-
  read_raw_data("../../Datos/2012/sect_a_c_d_e_pc_f_h_i_em_2012.dta")

data_15 <-
  read_raw_data("../../Datos/2015/sect_a_c_d_e_pc_f_h_i_2015.dta")


rmhas <- join_data(a_01, a_03, data_12, data_15)


describe_data(a_01)

export_data(rmhas, "data.csv")
