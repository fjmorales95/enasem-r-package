process_raw_data <-
  function(data_raw,
           keep_raw_data = F,
           md5sum = NULL) {
    np_converter <- function(x) {
      return(ifelse(x == 1, 10,
                    ifelse(x == 2, 20,
                           ifelse(x == 3, 11, 21))))
    }

    metadata <- list()
    data <- data_raw %>%
      rename_all(tolower)

    if (all(ID_NAMES_HOUSEHOLD_RAW$`2015` %in% names(data))) {
      attr(data, ATTR_YEARS) <- "2015"

      if (ID_NAMES_INDIVIDUAL_RAW$`2015` %in% names(data)) {
        data$np <- as.numeric(as.character(data$np))

        data$codent01 <- NULL
        data$codent03 <- NULL
      }

    } else if (all(ID_NAMES_HOUSEHOLD_RAW$`2012` %in% names(data))) {
      attr(data, ATTR_YEARS) <- "2012"

      if (ID_NAMES_INDIVIDUAL_RAW$`2012` %in% names(data)) {
        data$np <- as.numeric(as.character(data$np))

        data$codent01 <- NULL
        data$codent03 <- NULL
      }


    } else if (all(ID_NAMES_HOUSEHOLD_RAW$`2003` %in% names(data))) {
      attr(data, ATTR_YEARS) <- "2003"

      if (ID_NAMES_INDIVIDUAL_RAW$`2003` %in% names(data)) {
        data %>%
          rename_at(vars(ID_NAMES_INDIVIDUAL_RAW$`2003`), function(x)
            x = ID_NAMES_INDIVIDUAL) %>%
          mutate_at(vars(ID_NAMES_INDIVIDUAL), np_converter) ->
          data
      }

      identifiers <- intersect(names(data), IDS)

      data %>%
        rename_at(vars(-identifiers), paste0, "_03") %>%
        mutate(acthog = as.factor(as.numeric(as.character(acthog)))) ->
        data

    } else if (ID_NAMES_HOUSEHOLD_RAW$`2001` %in% names(data)) {
      attr(data, ATTR_YEARS) <- "2001"

      if (ID_NAMES_INDIVIDUAL_RAW$`2001` %in% names(data)) {
        data %>%
          rename_at(vars(ID_NAMES_INDIVIDUAL_RAW$`2001`), function(x)
            x = ID_NAMES_INDIVIDUAL) %>%
          mutate_at(vars(ID_NAMES_INDIVIDUAL), np_converter) ->
          data

      }

      identifiers <-
        intersect(names(data), c(IDS, ID_NAMES_HOUSEHOLD_RAW$`2001`))

      data %>%
        rename_at(vars(-identifiers), paste0, "_01") %>%
        rename_at(vars(ID_NAMES_HOUSEHOLD_RAW$`2001`), function(x)
          x = ID_NAMES_HOUSEHOLD$`2001`) ->
        data
    }

    attr(data, ATTR_TYPE) <- TYPE_DATA

    if (keep_raw_data)
      attr(data, ATTR_RAW_DATA) = data_raw

    return(data)
  }
