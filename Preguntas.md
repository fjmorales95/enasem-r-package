# Preguntas

+ ¿Cómo fueron construidas las variables de subhog? 
  + Basándose en la variable subhog_03, hay 68 "zombies" de 2003 a 2012. Cosa que no pasa si se toma en cuenta fallecido_03
+ Posibilidad de integrar el nombre (encriptado) de las tarjetas de registro y sección B para poder identificar a individuos a través de las olas.
  + ¿Cómo se captura cuando un individuo muere o cambia de casa fuera de la del entrevistado?
  + ¿Existen casos en los que una pareja separada registre como residente del hogar al mismo individuo?