# Notas

1. **NO SE PUEDE TRABAJAR CON B_DECEASED PORQUE NO HAY MANERA DE VINCULARLO CON UN INDIVIDUO DEL HOGAR. LO QUE SÌ SE PUEDE SABER ES SI ALGUNO MURIÓ GRACIAS A LAS SECCIONES B Y TRH.**
2. En 2001 no hay pregunta que diga si fue registrado por error el individuo. ¿Habrá casos en los que hayan sido registrados por error, ergo no sea correcto contarlo? ¿Qué hacer en este caso? Supongo que el registro estará lleno de nulos pues no se repondió nada, haciendo su eliminación un tanto "sencilla".
3. **Para el futuro**: Hacer el emparejamiento de preguntas en diferentes olas.
4. **No unió un dataset previo con nuevas**
5. ¿Los 50 que sobran de la unión entre trh_2001 y trh_2003 serán individuos repetidos para diferentes hogares?
   1. Hay 14 que parecen estar repetidos sin sentido alguno...
   2. 33 son repetidos porque se separó la pareja y ambos los registraron como residentes... Faltan 3
      1. CUNICAH 10055... Revisar sección B de este hogar.

# Pendientes en este documento

1. Describir los archivos del ENASEM
2. Enumerar las tablas una vez se haya terminado el documento
3. ¿Servirá de algo crear un ranking de identificadores para poder hacer uniones de tablas en la que ya hay distintos años? ¿O ya se hace por sí solo? Preguntas, preguntas y más preguntas...
4. **Reducir la dimensión de los data.frames haciendo el emperejamiento de las preguntas.** 
5. Describir unión de los archivos.



# Descripción de archivos del ENASEM

# Lectura de archivos

Actualmente el paquete *lee* archivos con extensión **.sps** (SPSS) usando una de las dos funciones dedicadas a la lectura. 

Para cada archivo se revisa la suma MD5 para comprobar si se trata del Archivo Maestro de Seguimiento (AMS) del 2015 ~~que hasta hace poco se usaba para todo~~, en caso contrario se pasa a procesamiento.

# Procesamiento de archivos

Lo primero al recibir un data.frame es determinar a qué año de la aplicación del instrumento pertenece. Se revisa desde el periodo más actual hasta el primer levantamiento si el data.frame contiene los identificadores de hogar correspondientes a cada año.

Los nombres de las columnas (variables) son pasadas a minúsculas y para los años 2001 y 2003 se agrega " \_01" y "\_03" respectivamente al final de todas las columnas que no correspondan a algún identificador.

Posteriormente, se siguen los siguientes puntos debido a la disparidad en los nombres y valores de los identificadores a lo largo de las etapas:

1. **Archivos de 2001**
   1. **ps3** se <u>tranforma</u> a **np** conforme a la *Tabla ps3-np*.
   2. **unhhid** se <u>renombra</u> a **cunicah **pues sólo difieren en significado.
   3. ~~Se le <u>agrega</u> **subhog_01** con valores igual a 0 para mantener coherencia con los años siguientes~~
2. **Archivos de 2003**
   1. **ent2** se <u>tranforma</u> a **np** conforme a la *Tabla ent2-np*.
   2. Se <u>eliminan</u> ceros a la izquierda en **acthog**.
3. **Archivos 2012**
   1. Se <u>elimina</u> **codent01** y **codent03** al no existir razón para conservarlas tras transformar *ps3* y *ent2*.
   2. Se <u>extraen</u> los primeros dos caracteres y se <u>eliminan</u> ceros a la izquierda de **acthog**, **subhog_01**, **subhog_03**, y **subhog_12** para eliminar la etiqueta del factor
4. **Archivos 2015**
   1. Se <u>elimina</u> **codent01** y **codent03 **por la misma razón de 3.1 en esta lista.
   2. Se <u>extraen</u> los últimos dos caracteres y se <u>eliminan</u> ceros a la izquierda de **acthog**, **subhog_01**, **subhog_03**, **subhog_12**, y **subhog_15** para eliminar la etiqueta del factor

# Tipos de tabla de datos crudos

Fueron encontrados tres tipos de tablas de acuerdo a lo que guarda cada registro (caso). 

## Descripción de columnas

La lista a continuación describe el nombre de las columnas que se usarán.

- **...ids_hogar...**: Repesenta a todas las variables que se ocupan para identificar un hogar. Estas pueden ser cunicah, subhog_03, etc.
- **np**: El Número de Persona a quien se refieren las variables.
- **...variables...**: Todas las columnas que no fueron identificadores. Estas pueden ser el tipo de entrevista, respuesta seleccionada a determinada pregunta, etc.

## Tipo I

El *Tipo I* guarda la información del sujeto de estudio. Usualmente son las respuestas a las preguntas e información capturada en determinada sección (o secciones) de la encuesta enfocada al individuo.

#### Esquema

| ..ids_hogar... |    np    | ...variables... |
| :------------: | :------: | :-------------: |
|    $\vdots$    | $\vdots$ |    $\vdots$     |

## Tipo II

El *Tipo II* guarda información del hogar de los sujetos de estudio. Usualmente son las respuestas proporcionadas en determinadas secciones del estudio.

#### Esquema

| ...ids_hogar... | ...variables... |
| :-------------: | :-------------: |
|    $\vdots$     |    $\vdots$     |

## Tipo III

El *Tipo III* guarda la información de cada residente del hogar e hijos del sujeto de estudio.

#### Esquema

| ...ids_hogar... | numero_registro | ...variables... |
| :-------------: | :-------------: | :-------------: |
|    $\vdots$     |    $\vdots$     |    $\vdots$     |

# Unión de data.frames

## Nivel Individuo

## Nivel Hogar

### Nivel Residente e Hijos

Pese a que la información de los residentes (TRH) e hijos (B) están almacenadas por hogar, para hacer la unión de los archivos se tiene que tomar en cuenta el número de registro del individuo y la condición de residencia - de ésta, en particular se necesita saber si el individuo fue listado por error, si es nuevo residente o salió del hogar entre la última aplicación y la *actual*; al contar individuos en estas situaciones resulta problemático pues no sería correcto contarlas al no tener razón de existir registrados o puden estar registrados dos veces, respectivamente. -. Aunado a eso, deja de ser una tarea trivial cuando se consideran casos como el siguiente:

~~**INSERTAR IMAGEN DEL PROBLEMÓN**~~

La imagen muestra 2 puntos importantes: 

+ El número de registro puede cambiar a lo largo de los años.
+ Dos registros diferentes en una tabla de *Tipo III* pueden referirse a sólo individuo.

El caso de la imagen representa dos registros con los siguiente valores.

| cunicah  |  acthog  |  reg_01  | $\dots$  |  reg_03  | $\dots$  |
| :------: | :------: | :------: | :------: | :------: | :------: |
| $\vdots$ | $\vdots$ | $\vdots$ | $\ddots$ | $\vdots$ | $\ddots$ |
|    x     |    y     |   10a    | $\dots$  |   10a    | $\dots$  |
| $\vdots$ | $\vdots$ | $\vdots$ | $\ddots$ | $\vdots$ | $\ddots$ |
|    x     |    y'    |   10a    | $\dots$  |   20b    | $\dots$  |
| $\vdots$ | $\vdots$ | $\vdots$ | $\ddots$ | $\vdots$ | $\ddots$ |

Es claro que este es un problema al no tener la certeza de que un renglón representa a un individuo único - e.g. en un conteo -. La solución propuesta es agregar una columna que haga referencia a la fila en el que apareció el individuo por primera vez y, de este modo, dejar al investigador el curso de acción hacía estos casos.

Ahora que he estado inspeccionando más el cuestionario, me doy cuenta de que esta duplicación de los individuos generé una reg_03 igual a reg_01 con la diferencia de que el individuo estará marcado como "ausente permanentemente" en algunos individuos. Es de mi conocimiento hasta este momento que estos individuos, que claremos: no son fallecidos, son no residentes pero mantienen el registro como si lo fueran. ¿Qué pasa en 2012 con estos individuos?



#### Preguntas de interés

##### Número de registro

+ **2001**: trh2, b3.
+ **2003**: trh3, b3, b25.
+ **2012 & 2015**: trh3, ntrh3, b3, nb3, b25, nb25.

##### Número de registro previo

+ **2003, 2012 & 2015**: trh6, b8.

##### Registro por error o cambio a vivienda de sujeto(s) de estudio.

+ **2003**: trh5:4, b7:4
+ **2012 & 2015**: trh5:4, b7:(4, 5)

**Es importante notar qué pasa cuando un individuo deja de vivir en la vivienda. Al parecer se marca como 2 en TRH5 pero no se pasa su información a la sección B (de 2001 a 2003).**

# Anexos

## Tablas

### Tabla ps3-np. Relación entre PS3 y NP

| PS3  |  NP  |
| :--: | :--: |
|  1   |  10  |
|  2   |  20  |

### Tabla ent2-np. Relación entre ENT2 y NP

| ENT2 |  NP  |
| :--: | :--: |
|  1   |  10  |
|  2   |  20  |
|  3   |  11  |
|  4   |  21  |

# Otros

## Tiempos de unión en diferentes computadores


```R
elapsed_time <- c()

for (i in 1:10) {
  print(i)
  begin <- Sys.time()
  merged <- join_data_per_individuals(m_ams, a_2001, a_2003, aa_2003, a_2012, a_2015)
  end <- Sys.time()
  
  elapsed_time[i] <- end - begin
} 

print(gettextf("mean: %f, sd: %f, median: %f", mean(elapsed_time), sd(elapsed_time), median(elapsed_time)))
"mean: 2.908212, sd: 0.058756, median: 2.886098"
```



```R
library(tidyverse)

elapsed_time <- c()

for (i in 1:5) {
  print(i)
  begin <- Sys.time()

  m_ams <-
    read_raw_ams(
      "../Datos enasem/master_follow_up_file_2015.sav",
      "../Datos enasem/master_follow_up_file_2015.sps"
    )

  a_2001 <- read_raw_data("../Datos enasem/2001/sect_a.sav")
  a_2003 <- read_raw_data("../Datos enasem/2003/sect_a.sav")
  aa_2003 <- read_raw_data("../Datos enasem/2003/sect_aa.sav")
  a_2012 <-
    read_raw_data("../Datos enasem/2012/sect_a_c_d_e_pc_f_h_i_em_2012.sav")
  a_2015 <-
    read_raw_data("../Datos enasem/2015/sect_a_c_d_e_pc_f_h_i_2015.sav")

  ids_mini <- m_ams[, IDS]

  a_2015 <
    a_2015 %>% select(starts_with("a"), cunicah, subhog_15, np) %>% head()
  a_2012 <-
    a_2012 %>% select(starts_with("a"), cunicah, subhog_12, np) %>% head()

  merged <-
    join_data_per_individuals(m_ams, a_2001, a_2003, aa_2003, a_2012, a_2015)
  end <- Sys.time()

  elapsed_time[i] <- end - begin
}

print(gettextf(
  "mean: %f, sd: %f, median: %f",
  mean(elapsed_time),
  sd(elapsed_time),
  median(elapsed_time)
))
"mean: 8.739464, sd: 0.126752, median: 8.718019"
```
fj
i5-4590 (3.3  Hz) &  
19.17, 0.14, 19.16