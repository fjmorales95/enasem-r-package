

****Correct label of variable mc_q49_16***
label variable mc_q49_16 "49: Lost consciousness result of the injury? Se desmayo o perdio el conocimiento"




****Consctructed variables Mex-Cog 2016***

*****************************
****DOMAIN 1: Orientation****
*****************************

***DAY Score***
gen D1_t1=1 if mc_q1_1_16==mc_q1_1c_16
replace D1_t1=0 if mc_q1_1_16 != mc_q1_1c_16
replace D1_t1=.r if mc_q1_1_16 == 99
label variable D1_t1 "D1_t1: Day of the month Dia del mes"
label define D1_t1 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso"
label values D1_t1 D1_t1

***MONTH Score***
gen D1_t2=1 if mc_q1_2_16==mc_q1_2c_16
replace D1_t2=0 if mc_q1_2_16 != mc_q1_2c_16 
replace D1_t2=.r if mc_q1_2_16 == 99
label variable D1_t2 "D1_t2: Month Mes"
label define D1_t2 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso"
label values D1_t2 D1_t2

***YEAR Score***
gen D1_t3=1 if mc_q1_3_16==mc_q1_3c_16 & mc_q1_3_16 <.
replace D1_t3=0 if mc_q1_3_16 != mc_q1_3c_16
replace D1_t3=.r if mc_q1_3_16 == 9999
label variable D1_t3 "D1_t3: Year Anio "
label define D1_t3 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso"
label values D1_t3 D1_t3

***DAYOFWEEK Score***
gen D1_t4=1 if mc_q2_c2_16==mc_q2_c2c_16 & mc_q2_c2_16 <.
replace D1_t4=0 if mc_q2_c2_16!=mc_q2_c2c_16  & mc_q2_c2_16 <.
replace D1_t4=0 if mc_q2_c2_16 == 8
replace D1_t4=.r if mc_q2_c2_16 == 9
label variable D1_t4 "D1_t4: Day of the week Dia de la semana"
label define D1_t4 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso"
label values D1_t4 D1_t4

***TIME Score***
***Whithin a 30 minute time frame***
gen subjecttime=mc_q3_1_16+(mc_q3_2_16/60)
gen correcttime=mc_q3_1c_16+(mc_q3_2c_16/60)
gen timedif=abs(correcttime-subjecttime)
gen D1_t5=1 if timedif <=.5
replace D1_t5=0 if timedif>.5
replace D1_t5=.r if mc_q3_1_16==99
drop subjecttime correcttime timedif
label variable D1_t5 "D1_t5: What time is it? Que hora es?"
label define D1_t5 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso"
label values D1_t5 D1_t5


***PLACE Score***
gen D1_t6=1 if mc_q4_16==1
replace D1_t6=0 if inlist(mc_q4_16,2,8)
replace D1_t6=.r if mc_q4_16 == 9
label variable D1_t6 "D1_t6: Where are we? Donde estamos?"
label define D1_t6 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso"
label values D1_t6 D1_t6

***Store***
gen D1_t7=1 if mc_q29_16==1
replace D1_t7=0 if inlist(mc_q29_16,2,8)
replace D1_t7=.r if mc_q29_16==9
replace D1_t7=.i if mc_q29_16==.i
label variable D1_t7 "D1_t7: How can I get to a store? Como llego a una tienda?"
label define D1_t7 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_t7 D1_t7

***Country Score***
gen D1_t8=1 if mc_q5_16==1
replace D1_t8=0 if inlist(mc_q5_16,2,8)
replace D1_t8=.r if mc_q5_16 == 9
label variable D1_t8 "D1_t8: What country are we in? En que pais estamos?"
label define D1_t8 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso"
label values D1_t8 D1_t8

***STATE Score***
gen D1_t9=1 if mc_q6_16==1
replace D1_t9=0 if inlist(mc_q6_16,2,8)
replace D1_t9=.r if mc_q6_16 == 9
label variable D1_t9 "D1_t9: What state are we in? En que estado estamos?"
label define D1_t9 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso"
label values D1_t9 D1_t9

				******************************
                ***Orientation domain score***
                ******************************

egen Orientation_correct=rowtotal( D1_t1-D1_t9)
replace Orientation_correct=.r if D1_t1==.r & D1_t2==.r & D1_t3==.r & D1_t4==.r & D1_t5==.r & D1_t6==.r & D1_t7==.r & D1_t8==.r & D1_t9==.r
label variable Orientation_correct "Orientation correct Orientacion correcto"
label define Orientation_correct .r ".r: Refused Rehuso"
label values Orientation_correct Orientation_correct

egen Orientation_attempts=rownonmiss( D1_t1-D1_t9)
replace Orientation_attempts=.r if D1_t1==.r & D1_t2==.r & D1_t3==.r & D1_t4==.r & D1_t5==.r & D1_t6==.r & D1_t7==.r & D1_t8==.r & D1_t9==.r
label variable Orientation_attempts "Orientation attempts Orientacion intentos"
label define Orientation_attempts .r ".r: Refused Rehuso"
label values Orientation_attempts Orientation_attempts



**********************************
****DOMAIN 2: Immediate memory****
**********************************


***IMMEDIATE WORD RECALL 1ST ATTEMPT Score***
gen iwr1=1 if mc_q7_1_16==1
replace iwr1=0 if inlist(mc_q7_1_16,2,8)
replace iwr1=.r if inlist(mc_q7_1_16,9)
replace iwr1=.i if mc_q7_1_16==.i
label variable iwr1 "(1) 3 word imm recall 3 palabras memoria inmediata"
label define iwr1 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values iwr1 iwr1


***IMMEDIATE WORD RECALL 2ND ATTEMPT Score***
gen iwr2=1 if mc_q7_2_16==1
replace iwr2=0 if inlist(mc_q7_2_16,2,8)
replace iwr2=.r if inlist(mc_q7_2_16,9)
replace iwr2=.i if mc_q7_2_16==.i
label variable iwr2 "(2) 3 word imm recall 3 palabras memoria inmediata"
label define iwr2 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values iwr2 iwr2

***IMMEDIATE WORD RECALL 3RD ATTEMPT Score***
gen iwr3=1 if mc_q7_3_16==1
replace iwr3=0 if inlist(mc_q7_3_16,2,8)
replace iwr3=.r if inlist(mc_q7_3_16,9)
replace iwr3=.i if mc_q7_3_16==.i
label variable iwr3 "(3) 3 word imm recall 3 palabras memoria inmediata"
label define iwr3 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values iwr3 iwr3

****Immediate 3 word recall***
egen D2_t1_correct=rowtotal(iwr1 iwr2 iwr3)
replace D2_t1_correct=.r if iwr1==.r & iwr2==.r & iwr3==.r
replace D2_t1_correct=.i if iwr1==.i 
label variable D2_t1_correct "Immediate recall 3 word correct Recuerdo inmediato 3 palabras correctos"
label define D2_t1_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_t1_correct D2_t1_correct

egen D2_t1_attempts=rownonmiss(iwr1 iwr2 iwr3)
replace D2_t1_attempts=.r if iwr1==.r & iwr2==.r & iwr3==.r
replace D2_t1_attempts=.i if iwr1==.i 
label variable D2_t1_attempts "Immediate recall 3 word attempts Recuerdo inmediato 3 palabras intentos"
label define D2_t1_attempts .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_t1_attempts D2_t1_attempts



***Word Recall 1st Attempt***
gen wr1_1=1 if mc_q18_1_1_16==1
replace wr1_1=0 if mc_q18_1_1_16==0
replace wr1_1=.i if mc_q18_1_1_16==.i
label variable wr1_1 "(1) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_1 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_1 wr1_1

gen wr1_2=1 if mc_q18_1_2_16==1
replace wr1_2=0 if mc_q18_1_2_16==0
replace wr1_2=.i if mc_q18_1_1_16==.i
label variable wr1_2 "(2) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_2 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_2 wr1_2

gen wr1_3=1 if mc_q18_1_3_16==1
replace wr1_3=0 if mc_q18_1_3_16==0
replace wr1_3=.i if mc_q18_1_1_16==.i
label variable wr1_3 "(3) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_3 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_3 wr1_3

gen wr1_4=1 if mc_q18_1_4_16==1
replace wr1_4=0 if mc_q18_1_4_16==0
replace wr1_4=.i if mc_q18_1_1_16==.i
label variable wr1_4 "(4) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_4 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_4 wr1_4

gen wr1_5=1 if mc_q18_1_5_16==1
replace wr1_5=0 if mc_q18_1_5_16==0
replace wr1_5=.i if mc_q18_1_1_16==.i
label variable wr1_5 "(5) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_5 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_5 wr1_5

gen wr1_6=1 if mc_q18_1_6_16==1
replace wr1_6=0 if mc_q18_1_6_16==0
replace wr1_6=.i if mc_q18_1_1_16==.i
label variable wr1_6 "(6) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_6 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_6 wr1_6

gen wr1_7=1 if mc_q18_1_7_16==1
replace wr1_7=0 if mc_q18_1_7_16==0
replace wr1_7=.i if mc_q18_1_1_16==.i
label variable wr1_7 "(7) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_7 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_7 wr1_7

gen wr1_8=1 if mc_q18_1_8_16==1
replace wr1_8=0 if mc_q18_1_8_16==0
replace wr1_8=.i if mc_q18_1_1_16==.i
label variable wr1_8 "(8) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_8 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_8 wr1_8

gen wr1_9=1 if mc_q18_1_9_16==1
replace wr1_9=0 if mc_q18_1_9_16==0
replace wr1_9=.i if mc_q18_1_1_16==.i
label variable wr1_9 "(9) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_9 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_9 wr1_9

gen wr1_10=1 if mc_q18_1_10_16==1
replace wr1_10=0 if mc_q18_1_10_16==0
replace wr1_10=.i if mc_q18_1_1_16==.i
label variable wr1_10 "(10) Round 1 10 Word recall Ronda 1 Memoria de 10 palabras"
label define wr1_10 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr1_10 wr1_10

****10 Word Recall Trial 1***
egen D2_t2_1_correct=rowtotal(wr1_1-wr1_10) 
replace D2_t2_1_correct=.i if wr1_1==.i & wr1_2==.i & wr1_3==.i & wr1_4==.i & wr1_5==.i & wr1_6==.i & wr1_7==.i & wr1_8==.i & wr1_9==.i & wr1_10==.i
label variable D2_t2_1_correct "D2_t2_1: Repeat 10 words Trial 1 Correct Repite 10 palabras ronda 1 correctos"
label define D2_t2_1_correct .i ".i: Incomplete Incompleto"
label values D2_t2_1_correct D2_t2_1_correct

egen D2_t2_1_attempts=rownonmiss(wr1_1-wr1_10) 
replace D2_t2_1_attempts=.i if wr1_1==.i & wr1_2==.i & wr1_3==.i & wr1_4==.i & wr1_5==.i & wr1_6==.i & wr1_7==.i & wr1_8==.i & wr1_9==.i & wr1_10==.i
label variable D2_t2_1_attempts "D2_t2_1: Repeat 10 words Trial 1 Attempts Repite 10 palabras ronda 1 intentos"
label define D2_t2_1_attempts .i ".i: Incomplete Incompleto"
label values D2_t2_1_attempts D2_t2_1_attempts

***Word Recall 2nd Attempt***
gen wr2_1=1 if mc_q19_2_1_16==1
replace wr2_1=0 if mc_q19_2_1_16==0
replace wr2_1=.i if mc_q18_1_1_16==.i
label variable wr2_1 "(1) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_1 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_1 wr2_1

gen wr2_2=1 if mc_q19_2_2_16==1
replace wr2_2=0 if mc_q19_2_2_16==0
replace wr2_2=.i if mc_q18_1_1_16==.i
label variable wr2_2 "(2) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_2 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_2 wr2_2

gen wr2_3=1 if mc_q19_2_3_16==1
replace wr2_3=0 if mc_q19_2_3_16==0
replace wr2_3=.i if mc_q18_1_1_16==.i
label variable wr2_3 "(3) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_3 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_3 wr2_3

gen wr2_4=1 if mc_q19_2_4_16==1
replace wr2_4=0 if mc_q19_2_4_16==0
replace wr2_4=.i if mc_q18_1_1_16==.i
label variable wr2_4 "(4) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_4 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_4 wr2_4

gen wr2_5=1 if mc_q19_2_5_16==1
replace wr2_5=0 if mc_q19_2_5_16==0
replace wr2_5=.i if mc_q18_1_1_16==.i
label variable wr2_5 "(5) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_5 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_5 wr2_5

gen wr2_6=1 if mc_q19_2_6_16==1
replace wr2_6=0 if mc_q19_2_6_16==0
replace wr2_6=.i if mc_q18_1_1_16==.i
label variable wr2_6 "(6) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_6 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_6 wr2_6

gen wr2_7=1 if mc_q19_2_7_16==1
replace wr2_7=0 if mc_q19_2_7_16==0
replace wr2_7=.i if mc_q18_1_1_16==.i
label variable wr2_7 "(7) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_7 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_7 wr2_7

gen wr2_8=1 if mc_q19_2_8_16==1
replace wr2_8=0 if mc_q19_2_8_16==0
replace wr2_8=.i if mc_q18_1_1_16==.i
label variable wr2_8 "(8) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_8 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_8 wr2_8

gen wr2_9=1 if mc_q19_2_9_16==1
replace wr2_9=0 if mc_q19_2_9_16==0
replace wr2_9=.i if mc_q18_1_1_16==.i
label variable wr2_9 "(9) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_9 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_9 wr2_9

gen wr2_10=1 if mc_q19_2_10_16==1
replace wr2_10=0 if mc_q19_2_10_16==0
replace wr2_10=.i if mc_q18_1_1_16==.i
label variable wr2_10 "(10) Round 2 10 Word recall Ronda 2 Memoria de 10 palabras"
label define wr2_10 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr2_10 wr2_10

****10 Word Recall Trial 2 ***
egen D2_t2_2_correct=rowtotal(wr2_1-wr2_10) 
replace D2_t2_2_correct=.i if wr2_1==.i & wr2_2==.i & wr2_3==.i & wr2_4==.i & wr2_5==.i & wr2_6==.i & wr2_7==.i & wr2_8==.i & wr2_9==.i & wr2_10==.i
label variable D2_t2_2_correct "D2_t2_2: Repeat 10 words Trial 2 Correct Repite 10 palabras ronda 2 correctos"
label define D2_t2_2_correct .i ".i: Incomplete Incompleto"
label values D2_t2_2_correct D2_t2_2_correct

egen D2_t2_2_attempts=rownonmiss(wr2_1-wr2_10) 
replace D2_t2_2_attempts=.i if wr2_1==.i & wr2_2==.i & wr2_3==.i & wr2_4==.i & wr2_5==.i & wr2_6==.i & wr2_7==.i & wr2_8==.i & wr2_9==.i & wr2_10==.i
label variable D2_t2_2_attempts "D2_t2_2: Repeat 10 words Trial 2 Attempts Repite 10 palabras ronda 2 intentos"
label define D2_t2_2_attempts .i ".i: Incomplete Incompleto"
label values D2_t2_2_attempts D2_t2_2_attempts

***Word Recall 3rd Attempt***
gen wr3_1=1 if mc_q20_3_1_16==1
replace wr3_1=0 if mc_q20_3_1_16==0
replace wr3_1=.i if mc_q18_1_1_16==.i
label variable wr3_1 "(1) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_1 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_1 wr3_1

gen wr3_2=1 if mc_q20_3_2_16==1
replace wr3_2=0 if mc_q20_3_2_16==0
replace wr3_2=.i if mc_q18_1_1_16==.i
label variable wr3_2 "(2) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_2 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_2 wr3_2

gen wr3_3=1 if mc_q20_3_3_16==1
replace wr3_3=0 if mc_q20_3_3_16==0
replace wr3_3=.i if mc_q18_1_1_16==.i
label variable wr3_3 "(3) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_3 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_3 wr3_3

gen wr3_4=1 if mc_q20_3_4_16==1
replace wr3_4=0 if mc_q20_3_4_16==0
replace wr3_4=.i if mc_q18_1_1_16==.i
label variable wr3_4 "(4) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_4 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_4 wr3_4

gen wr3_5=1 if mc_q20_3_5_16==1
replace wr3_5=0 if mc_q20_3_5_16==0
replace wr3_5=.i if mc_q18_1_1_16==.i
label variable wr3_5 "(5) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_5 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_5 wr3_5

gen wr3_6=1 if mc_q20_3_6_16==1
replace wr3_6=0 if mc_q20_3_6_16==0
replace wr3_6=.i if mc_q18_1_1_16==.i
label variable wr3_6 "(6) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_6 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_6 wr3_6

gen wr3_7=1 if mc_q20_3_7_16==1
replace wr3_7=0 if mc_q20_3_7_16==0
replace wr3_7=.i if mc_q18_1_1_16==.i
label variable wr3_7 "(7) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_7 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_7 wr3_7

gen wr3_8=1 if mc_q20_3_8_16==1
replace wr3_8=0 if mc_q20_3_8_16==0
replace wr3_8=.i if mc_q18_1_1_16==.i
label variable wr3_8 "(8) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_8 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_8 wr3_8

gen wr3_9=1 if mc_q20_3_9_16==1
replace wr3_9=0 if mc_q20_3_9_16==0
replace wr3_9=.i if mc_q18_1_1_16==.i
label variable wr3_9 "(9) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_9 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_9 wr3_9

gen wr3_10=1 if mc_q20_3_10_16==1
replace wr3_10=0 if mc_q20_3_10_16==0
replace wr3_10=.i if mc_q18_1_1_16==.i
label variable wr3_10 "(10) Round 3 10 Word recall Ronda 3 Memoria de 10 palabras"
label define wr3_10 1 "1: Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values wr3_10 wr3_10

****10 Word Recall total of trial 3 ***
egen D2_t2_3_correct=rowtotal(wr3_1-wr3_10) 
replace D2_t2_3_correct=.i if wr3_1==.i & wr3_2==.i & wr3_3==.i & wr3_4==.i & wr3_5==.i & wr3_6==.i & wr3_7==.i & wr3_8==.i & wr3_9==.i & wr3_10==.i
label variable D2_t2_3_correct "D2_t2_3: Repeat 10 words Trial 3 Correct Repite 10 palabras ronda 3 correctos"
label define D2_t2_3_correct .i ".i: Incomplete Incompleto"
label values D2_t2_3_correct D2_t2_3_correct

egen D2_t2_3_attempts=rownonmiss(wr3_1-wr3_10) 
replace D2_t2_3_attempts=.i if wr3_1==.i & wr3_2==.i & wr3_3==.i & wr3_4==.i & wr3_5==.i & wr3_6==.i & wr3_7==.i & wr3_8==.i & wr3_9==.i & wr3_10==.i
label variable D2_t2_3_attempts "D2_t2_3: Repeat 10 words Trial 3 Attempts Repite 10 palabras ronda 3 intentos"
label define D2_t2_3_attempts .i ".i: Incomplete Incompleto"
label values D2_t2_3_attempts D2_t2_3_attempts

****10 Word Recall total***
egen D2_t2_4=rowtotal(D2_t2_1_correct D2_t2_2_correct D2_t2_3_correct) 
replace D2_t2_4=.i if D2_t2_1_correct==.i & D2_t2_3_correct==.i & D2_t2_3_correct==.i
label variable D2_t2_4 "D2_t2_4: Repeat 10 words Total Repite 10 palabras total"
label define D2_t2_4 .i ".i: Incomplete Incompleto"
label values D2_t2_4 D2_t2_4

****10 Word Recall average***
gen D2_t2_5=(D2_t2_1_correct+D2_t2_1_correct+D2_t2_3_correct)/ 3
replace D2_t2_5=.i if D2_t2_1_correct==.i & D2_t2_3_correct==.i & D2_t2_3_correct==.i
label variable D2_t2_5 "D2_t2_5: Repeat 10 words Average Repite 10 palabras Promedio"
label define D2_t2_5 .i ".i: Incomplete Incompleto"
label values D2_t2_5 D2_t2_5

***Short story items***
gen shortstory1=mc_q32_1_16
label variable shortstory1 "Short story idea 1 Cuento corto idea 1"
label define shortstory1 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory1 shortstory1

gen shortstory2=mc_q32_2_16
label variable shortstory2 "Short story idea 2 Cuento corto idea 2"
label define shortstory2 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory2 shortstory2

gen shortstory3=mc_q32_3_16
label variable shortstory3 "Short story idea 3 Cuento corto idea 3"
label define shortstory3 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory3 shortstory3

gen shortstory4=mc_q32_4_16
label variable shortstory4 "Short story idea 4 Cuento corto idea 1"
label define shortstory4 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory4 shortstory4

gen shortstory5=mc_q32_5_16
label variable shortstory5 "Short story idea 5 Cuento corto idea 5"
label define shortstory5 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory5 shortstory5

gen shortstory6=mc_q32_6_16
label variable shortstory6 "Short story idea 6 Cuento corto idea 6"
label define shortstory6 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory6 shortstory6

***Short story total score***
egen D2_t3_1_correct=rowtotal(shortstory1-shortstory6) 
replace D2_t3_1_correct=.i if mc_q32_1_16==.i
replace D2_t3_1_correct=.c if mc_q32_1_16==.c
label variable D2_t3_1_correct "D2_t3 Immediate recall of short story score Recuerdo inmediato de cuento corto puntuacion"
label define D2_t3_1_correct .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_t3_1_correct D2_t3_1_correct

egen D2_t3_1_attempts=rownonmiss(shortstory1-shortstory6) 
replace D2_t3_1_attempts= D2_t3_1_attempts*2
replace D2_t3_1_attempts=.i if mc_q32_1_16==.i
replace D2_t3_1_attempts=.c if mc_q32_1_16==.c
label variable D2_t3_1_attempts "D2_t3 Immediate recall of short story attempts Recuerdo inmediato de cuento corto intentos"
label define D2_t3_1_attempts .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_t3_1_attempts D2_t3_1_attempts

***Short story approximate***
gen shortstory1_a =1 if inlist(mc_q32_1_16,1,2)
replace shortstory1_a=0 if mc_q32_1_16==0
label variable shortstory1_a "Approx Short story idea 1 Cuento corto idea 1"
label define shortstory1_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory1_a shortstory1_a

gen shortstory2_a =1 if inlist(mc_q32_2_16,1,2)
replace shortstory2_a=0 if mc_q32_2_16==0
label variable shortstory2_a "Approx Short story idea 2 Cuento corto idea 2"
label define shortstory2_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory2_a shortstory2_a

gen shortstory3_a =1 if inlist(mc_q32_3_16,1,2)
replace shortstory3_a=0 if mc_q32_3_16==0
label variable shortstory3_a "Approx Short story idea 3 Cuento corto idea 3"
label define shortstory3_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory3_a shortstory3_a

gen shortstory4_a =1 if inlist(mc_q32_4_16,1,2)
replace shortstory4_a =0 if mc_q32_4_16==0
label variable shortstory4_a "Approx Short story idea 4 Cuento corto idea 4"
label define shortstory4_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory4_a shortstory4_a

gen shortstory5_a =1 if inlist(mc_q32_5_16,1,2)
replace shortstory5_a =0 if mc_q32_5_16==0
label variable shortstory5_a "Approx Short story idea 5 Cuento corto idea 5"
label define shortstory5_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory5_a shortstory5_a

gen shortstory6_a =1 if inlist(mc_q32_6_16,1,2)
replace shortstory6_a =0 if mc_q32_5_16==0
label variable shortstory6_a "Approx Short story idea 6 Cuento corto idea 6"
label define shortstory6_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory6_a shortstory6_a

***Short story aproximmate score***
egen D2_t3_2= rowtotal(shortstory1_a-shortstory6_a)
replace D2_t3_2=.i if mc_q32_1_16==.i
replace D2_t3_2=.c if mc_q32_1_16==.c
label variable D2_t3_2 "D2_t3_2:Immediate recall of short story approximate Recuerdo inmediato de cuento corto aproximado"
label define D2_t3_2 .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_t3_2 D2_t3_2

***Short story exact***
gen shortstory1_e =0 if inlist(mc_q32_1_16,0,1)
replace shortstory1_e=1 if mc_q32_1_16==2
label variable shortstory1_e "Exact Short story idea 1 Cuento corto idea 1"
label define shortstory1_e 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory1_e shortstory1_e

gen shortstory2_e =0 if inlist(mc_q32_2_16,0,1)
replace shortstory2_e =1 if mc_q32_2_16==2
label variable shortstory2_e "Exact Short story idea 2 Cuento corto idea 2"
label define shortstory2_e 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory2_e shortstory2_e

gen shortstory3_e =0 if inlist(mc_q32_3_16,0,1)
replace shortstory3_e =1 if mc_q32_3_16==2
label variable shortstory3_e "Exact Short story idea 3 Cuento corto idea 3"
label define shortstory3_e 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory3_e shortstory3_e

gen shortstory4_e =0 if inlist(mc_q32_4_16,0,1)
replace shortstory4_e =1 if mc_q32_4_16==2
label variable shortstory4_e "Exact Short story idea 4 Cuento corto idea 4"
label define shortstory4_e 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory4_e shortstory4_e

gen shortstory5_e =0 if inlist(mc_q32_5_16,0,1)
replace shortstory5_e =1 if mc_q32_5_16==2
label variable shortstory5_e "Exact Short story idea 5 Cuento corto idea 5"
label define shortstory5_e 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory5_e shortstory5_e

gen shortstory6_e =0 if inlist(mc_q32_6_16,0,1)
replace shortstory6_e =1 if mc_q32_6_16==2
label variable shortstory6_e "Exact Short story idea 6 Cuento corto idea 6"
label define shortstory6_e 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory6_e shortstory6_e

***Short story exact score***
egen D2_t3_3 = rowtotal(shortstory1_e - shortstory6_e)
replace D2_t3_3=.i if mc_q32_1_16==.i
replace D2_t3_3=.c if mc_q32_1_16==.c
label variable D2_t3_3 "D2_t3_3:Immediate recall of short story exact Recuerdo inmediato de cuento corto exacto"
label define D2_t3_3 .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_t3_3 D2_t3_3

***Long story items***
gen longstory1=mc_q33_1_16
label variable longstory1 "Long story idea 1 Cuento largo idea 1"
label define longstory1 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory1 longstory1

gen longstory2=mc_q33_2_16
label variable longstory2 "Long story idea 2 Cuento largo idea 2"
label define longstory2 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory2 longstory2

gen longstory3=mc_q33_3_16
label variable longstory3 "Long story idea 3 Cuento largo idea 3"
label define longstory3 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory3 longstory1

gen longstory4=mc_q33_4_16
label variable longstory4 "Long story idea 4 Cuento largo idea 4"
label define longstory4 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory4 longstory4

gen longstory5=mc_q33_5_16
label variable longstory5 "Long story idea 5 Cuento largo idea 5"
label define longstory5 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory5 longstory5

gen longstory6=mc_q33_6_16
label variable longstory6 "Long story idea 6 Cuento largo idea 6"
label define longstory6 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory6 longstory6

gen longstory7=mc_q33_7_16
label variable longstory7 "Long story idea 7 Cuento largo idea 7"
label define longstory7 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory7 longstory7

gen longstory8=mc_q33_8_16
label variable longstory8 "Long story idea 8 Cuento largo idea 8"
label define longstory8 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory8 longstory8

gen longstory9=mc_q33_9_16
label variable longstory9 "Long story idea 9 Cuento largo idea 9"
label define longstory9 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory9 longstory9

gen longstory10=mc_q33_10_16
label variable longstory10 "Long story idea 10 Cuento largo idea 10"
label define longstory10 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory10 longstory10

gen longstory11=mc_q33_11_16
label variable longstory11 "Long story idea 11 Cuento largo idea 11"
label define longstory11 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory11 longstory11

gen longstory12=mc_q33_12_16
label variable longstory12 "Long story idea 12 Cuento largo idea 12"
label define longstory12 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory12 longstory12

gen longstory13=mc_q33_13_16
label variable longstory13 "Long story idea 13 Cuento largo idea 13"
label define longstory13 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory13 longstory13

gen longstory14=mc_q33_14_16
label variable longstory14 "Long story idea 14 Cuento largo idea 14"
label define longstory14 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory14 longstory14

gen longstory15=mc_q33_15_16
label variable longstory15 "Long story idea 15 Cuento largo idea 15"
label define longstory15 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory15 longstory15

gen longstory16=mc_q33_16_16
label variable longstory16 "Long story idea 16 Cuento largo idea 16"
label define longstory16 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory16 longstory16

gen longstory17=mc_q33_17_16
label variable longstory17 "Long story idea 17 Cuento largo idea 17"
label define longstory17 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory17 longstory17

gen longstory18=mc_q33_18_16
label variable longstory18 "Long story idea 18 Cuento largo idea 18"
label define longstory18 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory18 longstory18

gen longstory19=mc_q33_19_16
label variable longstory19 "Long story idea 19 Cuento largo idea 19"
label define longstory19 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory19 longstory19

gen longstory20=mc_q33_20_16
label variable longstory20 "Long story idea 20 Cuento largo idea 20"
label define longstory20 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory20 longstory20

gen longstory21=mc_q33_21_16
label variable longstory21 "Long story idea 21 Cuento largo idea 21"
label define longstory21 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory21 longstory21

gen longstory22=mc_q33_22_16
label variable longstory22 "Long story idea 22 Cuento largo idea 22"
label define longstory22 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory22 longstory22

gen longstory23=mc_q33_23_16
label variable longstory23 "Long story idea 23 Cuento largo idea 23"
label define longstory23 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory23 longstory23

gen longstory24=mc_q33_24_16
label variable longstory24 "Long story idea 24 Cuento largo idea 24"
label define longstory24 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory24 longstory24

gen longstory25=mc_q33_25_16
label variable longstory25 "Long story idea 25 Cuento largo idea 25"
label define longstory25 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory25 longstory25


***Long story correct***
egen D2_t4_1_correct=rowtotal(longstory1-longstory25)
replace D2_t4_1_correct=.i if mc_q33_1_16==.i
replace D2_t4_1_correct=.c if mc_q33_1_16==.c
label variable D2_t4_1_correct "D2_t4_1: Immediate recall of long story correct Recuerdo inmediato de cuento largo correcto"
label define D2_t4_1_correct .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_t4_1_correct D2_t4_1_correct

***Long story approximate attemps***
egen D2_t4_1_attempts=rownonmiss(longstory1-longstory25) 
replace D2_t4_1_attempts = D2_t4_1_attempts*2 
replace D2_t4_1_attempts=.i if mc_q33_1_16==.i
replace D2_t4_1_attempts=.c if mc_q33_1_16==.c
label variable D2_t4_1_attempts "D2_t4_1: Immediate recall of long story attempts Recuerdo inmediato de cuento largo intentos"
label define D2_t4_1_attempts .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_t4_1_attempts D2_t4_1_attempts

***Long story approximate items***
gen longstory1_a =1 if inlist(mc_q33_1_16,1,2)
replace longstory1_a=0 if mc_q33_1_16==0
label variable longstory1_a "Approx Long story idea 1 Cuento largo idea 1"
label define longstory1_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory1_a longstory1_a

gen longstory2_a =1 if inlist(mc_q33_2_16,1,2)
replace longstory2_a=0 if mc_q33_2_16==0
label variable longstory2_a "Approx Long story idea 2 Cuento largo idea 2"
label define longstory2_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory2_a longstory2_a

gen longstory3_a =1 if inlist(mc_q33_3_16,1,2)
replace longstory3_a=0 if mc_q33_3_16==0
label variable longstory3_a "Approx Long story idea 3 Cuento largo idea 3"
label define longstory3_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory3_a longstory3_a

gen longstory4_a =1 if inlist(mc_q33_4_16,1,2)
replace longstory4_a=0 if mc_q33_4_16==0
label variable longstory4_a "Approx Long story idea 4 Cuento largo idea 4"
label define longstory4_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory4_a longstory4_a

gen longstory5_a =1 if inlist(mc_q33_5_16,1,2)
replace longstory5_a=0 if mc_q33_5_16==0
label variable longstory5_a "Approx Long story idea 5 Cuento largo idea 5"
label define longstory5_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory5_a longstory5_a

gen longstory6_a =1 if inlist(mc_q33_6_16,1,2)
replace longstory6_a=0 if mc_q33_6_16==0
label variable longstory6_a "Approx Long story idea 6 Cuento largo idea 6"
label define longstory6_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory6_a longstory6_a

gen longstory7_a =1 if inlist(mc_q33_7_16,1,2)
replace longstory7_a=0 if mc_q33_7_16==0
label variable longstory7_a "Approx Long story idea 7 Cuento largo idea 7"
label define longstory7_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory7_a longstory7_a

gen longstory8_a =1 if inlist(mc_q33_8_16,1,2)
replace longstory8_a=0 if mc_q33_8_16==0
label variable longstory8_a "Approx Long story idea 8 Cuento largo idea 8"
label define longstory8_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory8_a longstory8_a

gen longstory9_a =1 if inlist(mc_q33_9_16,1,2)
replace longstory9_a=0 if mc_q33_9_16==0
label variable longstory9_a "Approx Long story idea 9 Cuento largo idea 9"
label define longstory9_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory9_a longstory9_a

gen longstory10_a =1 if inlist(mc_q33_10_16,1,2)
replace longstory10_a=0 if mc_q33_10_16==0
label variable longstory10_a "Approx Long story idea 10 Cuento largo idea 10"
label define longstory10_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory10_a longstory10_a

gen longstory11_a =1 if inlist(mc_q33_11_16,1,2)
replace longstory11_a=0 if mc_q33_11_16==0
label variable longstory11_a "Approx Long story idea 11 Cuento largo idea 11"
label define longstory11_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory11_a longstory11_a

gen longstory12_a =1 if inlist(mc_q33_12_16,1,2)
replace longstory12_a=0 if mc_q33_12_16==0
label variable longstory12_a "Approx Long story idea 12 Cuento largo idea 12"
label define longstory12_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory12_a longstory12_a

gen longstory13_a =1 if inlist(mc_q33_13_16,1,2)
replace longstory13_a=0 if mc_q33_13_16==0
label variable longstory13_a "Approx Long story idea 13 Cuento largo idea 13"
label define longstory13_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory13_a longstory13_a

gen longstory14_a =1 if inlist(mc_q33_14_16,1,2)
replace longstory14_a=0 if mc_q33_14_16==0
label variable longstory14_a "Approx Long story idea 14 Cuento largo idea 14"
label define longstory14_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory14_a longstory14_a

gen longstory15_a =1 if inlist(mc_q33_15_16,1,2)
replace longstory15_a=0 if mc_q33_15_16==0
label variable longstory15_a "Approx Long story idea 15 Cuento largo idea 15"
label define longstory15_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory15_a longstory15_a

gen longstory16_a =1 if inlist(mc_q33_16_16,1,2)
replace longstory16_a=0 if mc_q33_16_16==0
label variable longstory16_a "Approx Long story idea 16 Cuento largo idea 16"
label define longstory16_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory16_a longstory16_a

gen longstory17_a =1 if inlist(mc_q33_17_16,1,2)
replace longstory17_a=0 if mc_q33_17_16==0
label variable longstory17_a "Approx Long story idea 17 Cuento largo idea 17"
label define longstory17_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory17_a longstory17_a

gen longstory18_a =1 if inlist(mc_q33_18_16,1,2)
replace longstory18_a=0 if mc_q33_18_16==0
label variable longstory18_a "Approx Long story idea 18 Cuento largo idea 18"
label define longstory18_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory18_a longstory18_a

gen longstory19_a =1 if inlist(mc_q33_19_16,1,2)
replace longstory19_a=0 if mc_q33_19_16==0
label variable longstory19_a "Approx Long story idea 19 Cuento largo idea 19"
label define longstory19_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory19_a longstory19_a

gen longstory20_a =1 if inlist(mc_q33_20_16,1,2)
replace longstory20_a=0 if mc_q33_20_16==0
label variable longstory20_a "Approx Long story idea 20 Cuento largo idea 20"
label define longstory20_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory20_a longstory20_a

gen longstory21_a =1 if inlist(mc_q33_21_16,1,2)
replace longstory21_a=0 if mc_q33_21_16==0
label variable longstory21_a "Approx Long story idea 21 Cuento largo idea 21"
label define longstory21_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory21_a longstory21_a

gen longstory22_a =1 if inlist(mc_q33_22_16,1,2)
replace longstory22_a=0 if mc_q33_22_16==0
label variable longstory22_a "Approx Long story idea 22 Cuento largo idea 22"
label define longstory22_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory22_a longstory22_a

gen longstory23_a =1 if inlist(mc_q33_23_16,1,2)
replace longstory23_a=0 if mc_q33_23_16==0
label variable longstory23_a "Approx Long story idea 23 Cuento largo idea 23"
label define longstory23_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory23_a longstory23_a

gen longstory24_a =1 if inlist(mc_q33_24_16,1,2)
replace longstory24_a=0 if mc_q33_24_16==0
label variable longstory24_a "Approx Long story idea 24 Cuento largo idea 24"
label define longstory24_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory24_a longstory24_a

gen longstory25_a =1 if inlist(mc_q33_25_16,1,2)
replace longstory25_a=0 if mc_q33_25_16==0
label variable longstory25_a "Approx Long story idea 25 Cuento largo idea 25"
label define longstory25_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory25_a longstory25_a


***Long story approximate score***
egen D2_t4_2 = rowtotal(longstory1_a - longstory25_a)
replace D2_t4_2=.i if mc_q33_1_16==.i
replace D2_t4_2=.c if mc_q33_1_16==.c
label variable D2_t4_2 "D2_t4_2: Immediate recall of long story approximate Recuerdo inmediato de cuento largo aproximado"
label define D2_t4_2 .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_t4_2 D2_t4_2

***Long story exact ***
gen longstory1_e =1 if mc_q33_1_16==2
replace longstory1_e=0 if inlist(mc_q33_1_16,0,1)
label variable longstory1_e "Exact Long story idea 1 Cuento largo idea 1"
label define longstory1_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory1_e longstory1_e

gen longstory2_e =1 if mc_q33_2_16==2
replace longstory2_e=0 if inlist(mc_q33_2_16,0,1)
label variable longstory2_e "Exact Long story idea 2 Cuento largo idea 2"
label define longstory2_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory2_e longstory2_e

gen longstory3_e =1 if mc_q33_3_16==2
replace longstory3_e=0 if inlist(mc_q33_3_16,0,1)
label variable longstory3_e "Exact Long story idea 3 Cuento largo idea 3"
label define longstory3_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory3_e longstory3_e

gen longstory4_e =1 if mc_q33_4_16==2
replace longstory4_e=0 if inlist(mc_q33_4_16,0,1)
label variable longstory4_e "Exact Long story idea 4 Cuento largo idea 4"
label define longstory4_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory4_e longstory4_e

gen longstory5_e =1 if mc_q33_5_16==2
replace longstory5_e=0 if inlist(mc_q33_5_16,0,1)
label variable longstory5_e "Exact Long story idea 5 Cuento largo idea 5"
label define longstory5_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory5_e longstory5_e

gen longstory6_e =1 if mc_q33_6_16==2
replace longstory6_e=0 if inlist(mc_q33_6_16,0,1)
label variable longstory6_e "Exact Long story idea 6 Cuento largo idea 6"
label define longstory6_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory6_e longstory6_e

gen longstory7_e =1 if mc_q33_7_16==2
replace longstory7_e=0 if inlist(mc_q33_7_16,0,1)
label variable longstory7_e "Exact Long story idea 7 Cuento largo idea 7"
label define longstory7_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory7_e longstory7_e

gen longstory8_e =1 if mc_q33_8_16==2
replace longstory8_e=0 if inlist(mc_q33_8_16,0,1)
label variable longstory8_e "Exact Long story idea 8 Cuento largo idea 8"
label define longstory8_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory8_e longstory8_e

gen longstory9_e =1 if mc_q33_9_16==2
replace longstory9_e=0 if inlist(mc_q33_9_16,0,1)
label variable longstory9_e "Exact Long story idea 9 Cuento largo idea 9"
label define longstory9_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory9_e longstory9_e

gen longstory10_e =1 if mc_q33_10_16==2
replace longstory10_e=0 if inlist(mc_q33_10_16,0,1)
label variable longstory10_e "Exact Long story idea 10 Cuento largo idea 10"
label define longstory10_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory10_e longstory10_e

gen longstory11_e =1 if mc_q33_11_16==2
replace longstory11_e=0 if inlist(mc_q33_11_16,0,1)
label variable longstory11_e "Exact Long story idea 11 Cuento largo idea 11"
label define longstory11_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory11_e longstory11_e

gen longstory12_e =1 if mc_q33_12_16==2
replace longstory12_e=0 if inlist(mc_q33_12_16,0,1)
label variable longstory12_e "Exact Long story idea 12 Cuento largo idea 12"
label define longstory12_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory12_e longstory12_e

gen longstory13_e =1 if mc_q33_13_16==2
replace longstory13_e=0 if inlist(mc_q33_13_16,0,1)
label variable longstory13_e "Exact Long story idea 13 Cuento largo idea 13"
label define longstory13_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory13_e longstory13_e

gen longstory14_e =1 if mc_q33_14_16==2
replace longstory14_e=0 if inlist(mc_q33_14_16,0,1)
label variable longstory14_e "Exact Long story idea 14 Cuento largo idea 14"
label define longstory14_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory14_e longstory14_e

gen longstory15_e =1 if mc_q33_15_16==2
replace longstory15_e=0 if inlist(mc_q33_15_16,0,1)
label variable longstory15_e "Exact Long story idea 15 Cuento largo idea 15"
label define longstory15_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory15_e longstory15_e

gen longstory16_e =1 if mc_q33_16_16==2
replace longstory16_e=0 if inlist(mc_q33_16_16,0,1)
label variable longstory16_e "Exact Long story idea 16 Cuento largo idea 16"
label define longstory16_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory16_e longstory16_e

gen longstory17_e =1 if mc_q33_17_16==2
replace longstory17_e=0 if inlist(mc_q33_17_16,0,1)
label variable longstory17_e "Exact Long story idea 17 Cuento largo idea 17"
label define longstory17_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory17_e longstory17_e

gen longstory18_e =1 if mc_q33_18_16==2
replace longstory18_e =0 if inlist(mc_q33_18_16,0,1)
label variable longstory18_e "Exact Long story idea 18 Cuento largo idea 18"
label define longstory18_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory18_e longstory18_e

gen longstory19_e =1 if mc_q33_19_16==2
replace longstory19_e =0 if inlist(mc_q33_19_16,0,1)
label variable longstory19_e "Exact Long story idea 19 Cuento largo idea 19"
label define longstory19_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory19_e longstory19_e

gen longstory20_e =1 if mc_q33_20_16==2
replace longstory20_e=0 if inlist(mc_q33_20_16,0,1)
label variable longstory20_e "Exact Long story idea 20 Cuento largo idea 20"
label define longstory20_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory20_e longstory20_e

gen longstory21_e =1 if mc_q33_21_16==2
replace longstory21_e =0 if inlist(mc_q33_21_16,0,1)
label variable longstory21_e "Exact Long story idea 21 Cuento largo idea 21"
label define longstory21_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory21_e longstory21_e

gen longstory22_e =1 if mc_q33_22_16==2
replace longstory22_e =0 if inlist(mc_q33_22_16,0,1)
label variable longstory22_e "Exact Long story idea 22 Cuento largo idea 22"
label define longstory22_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory22_e longstory22_e

gen longstory23_e =1 if mc_q33_23_16==2
replace longstory23_e=0 if inlist(mc_q33_23_16,0,1)
label variable longstory23_e "Exact Long story idea 23 Cuento largo idea 23"
label define longstory23_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory23_e longstory23_e

gen longstory24_e =1 if mc_q33_24_16==2
replace longstory24_e=0 if inlist(mc_q33_24_16,0,1)
label variable longstory24_e "Exact Long story idea 24 Cuento largo idea 24"
label define longstory24_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory24_e longstory24_e

gen longstory25_e =1 if mc_q33_25_16==2
replace longstory25_e=0 if inlist(mc_q33_25_16,0,1)
label variable longstory25_e "Exact Long story idea 25 Cuento largo idea 25"
label define longstory25_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory25_e longstory25_e

***Long story exact score***
egen D2_t4_3 = rowtotal(longstory1_e-longstory25_e)
replace D2_t4_3=.i if mc_q33_1_16==.i
replace D2_t4_3=.c if mc_q33_1_16==.c
label variable D2_t4_3 "D2_t4_2: Immediate recall of long story exact Recuerdo inmediato de cuento largo exacto"
label define D2_t4_3 .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_t4_3 D2_t4_3


				************************************
				***Immediate Memory domain score****
				************************************
egen D2_ImmediateMemory_correct=rowtotal(D2_t1_correct D2_t2_1_correct D2_t2_2_correct D2_t2_3_correct D2_t3_1_correct D2_t4_1_correct) 
replace D2_ImmediateMemory_correct=.i if D2_t1_correct==.i & D2_t2_1_correct==.i & D2_t2_2_correct==.i & D2_t2_3_correct==.i & D2_t3_1_correct==.i & D2_t4_1_correct==.i
replace D2_ImmediateMemory_correct=.c if D2_t1_correct==.c & D2_t2_1_correct==.c & D2_t2_2_correct==.c & D2_t2_3_correct==.c & D2_t3_1_correct==.c & D2_t4_1_correct==.c
label variable D2_ImmediateMemory_correct "D2: Immediate Memory correct Memoria inmediata correcto"
label define D2_ImmediateMemory_correct .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_ImmediateMemory_correct D2_ImmediateMemory_correct

egen D2_ImmediateMemory_attempts=rowtotal(D2_t1_attempts D2_t2_1_attempts D2_t2_2_attempts D2_t2_3_attempts D2_t3_1_attempts D2_t4_1_attempts) 
replace D2_ImmediateMemory_attempts=.i if D2_t1_attempts==.i & D2_t2_1_attempts==.i & D2_t2_2_attempts==.i & D2_t2_3_attempts==.i & D2_t3_1_attempts==.i & D2_t4_1_attempts==.i
replace D2_ImmediateMemory_attempts=.c if D2_t1_attempts==.c & D2_t2_1_attempts==.c & D2_t2_2_attempts==.c & D2_t2_3_attempts==.c & D2_t3_1_attempts==.c & D2_t4_1_attempts==.c
label variable D2_ImmediateMemory_attempts "D2: Immediate Memory atempts Memoria inmediata intentos"
label define D2_ImmediateMemory_attempts .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D2_ImmediateMemory_attempts D2_ImmediateMemory_attempts


***DELAYED WORD RECALL 1ST ATTEMPT***
gen dwr1=1 if mc_q10_1_16==1
replace dwr1=0 if inlist(mc_q10_1_16,2,8)
replace dwr1=.r if inlist(mc_q10_1_16,9)
replace dwr1=.i if inlist(mc_q10_1_16,.i)
label variable dwr1 "(1) 3 word delayed recall 3 palabras memoria diferida"
label define dwr1 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values dwr1 dwr1

***DELAYED WORD RECALL 2ND ATTEMPT***
gen dwr2=1 if mc_q10_2_16==1
replace dwr2=0 if inlist(mc_q10_2_16,2,8)
replace dwr2=.r if inlist(mc_q10_2_16,9)
replace dwr2=.i if inlist(mc_q10_1_16,.i)
label variable dwr2 "(2) 3 word delayed recall 3 palabras memoria diferida"
label define dwr2 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values dwr2 dwr2

***DELAYED WORD RECALL 3RD ATTEMPT ***
gen dwr3=1 if mc_q10_3_16==1
replace dwr3=0 if inlist(mc_q10_3_16,2,8)
replace dwr3=.r if inlist(mc_q10_3_16,9)
replace dwr3=.i if inlist(mc_q10_1_16,.i)
label variable dwr3 "(3) 3 word delayed recall 3 palabras memoria diferida"
label define dwr3 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values dwr3 dwr3

****Delayed 3 word recall***
egen D3_t1_correct=rowtotal(dwr1 dwr2 dwr3)
replace D3_t1_correct=.r if dwr1==.r & dwr2==.r & dwr3==.r
replace D3_t1_correct=.i if dwr1==.i & dwr2==.i & dwr3==.i
label variable D3_t1_correct "D3_t1: Delayed recall of three words correct 3 Recuerdo diferido de  palabras correcto"
label define D3_t1_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D3_t1_correct D3_t1_correct

egen D3_t1_attempts=rownonmiss(dwr1 dwr2 dwr3)
replace D3_t1_attempts=.r if dwr1==.r & dwr2==.r & dwr3==.r
replace D3_t1_attempts=.i if dwr1==.i & dwr2==.i & dwr3==.i
label variable D3_t1_attempts "D3_t1: Delayed recall of three words attempts 3 Recuerdo diferido de  palabras intentos"
label define D3_t1_attempts .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D3_t1_attempts D3_t1_attempts

***Word Recall attemps***
gen recall1=1 if mc_q31_1_1_16==1
replace recall1=0 if mc_q31_1_1_16==0
replace recall1=.i if mc_q31_1_1_16==.i
label variable recall1 "(1) 10 Word recall Memoria 10 palabras"
label define recall1 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall1 recall1

gen recall2=1 if mc_q31_1_2_16==1
replace recall2=0 if mc_q31_1_2_16==0
replace recall2=.i if mc_q31_1_1_16==.i
label variable recall2 "(2) 10 Word recall Memoria 10 palabras"
label define recall2 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall2 recall2

gen recall3=1 if mc_q31_1_3_16==1
replace recall3=0 if mc_q31_1_3_16==0
replace recall3=.i if mc_q31_1_1_16==.i
label variable recall3 "(3) 10 Word recall Memoria 10 palabras"
label define recall3 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall3 recall3

gen recall4=1 if mc_q31_1_4_16==1
replace recall4=0 if mc_q31_1_4_16==0
replace recall4=.i if mc_q31_1_1_16==.i
label variable recall4 "(4) 10 Word recall Memoria 10 palabras"
label define recall4 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall4 recall4

gen recall5=1 if mc_q31_1_5_16==1
replace recall5=0 if mc_q31_1_5_16==0
replace recall5=.i if mc_q31_1_1_16==.i
label variable recall5 "(5) 10 Word recall Memoria 10 palabras"
label define recall5 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall5 recall5

gen recall6=1 if mc_q31_1_6_16==1
replace recall6=0 if mc_q31_1_6_16==0
replace recall6=.i if mc_q31_1_1_16==.i
label variable recall6 "(6) 10 Word recall Memoria 10 palabras"
label define recall6 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall6 recall6

gen recall7=1 if mc_q31_1_7_16==1
replace recall7=0 if mc_q31_1_7_16==0
replace recall7=.i if mc_q31_1_1_16==.i
label variable recall7 "(7) 10 Word recall Memoria 10 palabras"
label define recall7 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall7 recall7

gen recall8=1 if mc_q31_1_8_16==1
replace recall8=0 if mc_q31_1_8_16==0
replace recall8=.i if mc_q31_1_1_16==.i
label variable recall8 "(8) 10 Word recall Memoria 10 palabras"
label define recall8 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall8 recall8

gen recall9=1 if mc_q31_1_9_16==1
replace recall9=0 if mc_q31_1_9_16==0
replace recall9=.i if mc_q31_1_1_16==.i
label variable recall9 "(9) 10 Word recall Memoria 10 palabras"
label define recall9 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall9 recall9

gen recall10=1 if mc_q31_1_10_16==1
replace recall10=0 if mc_q31_1_10_16==0
replace recall10=.i if mc_q31_1_1_16==.i
label variable recall10 "(10) 10 Word recall Memoria 10 palabras"
label define recall10 1 "1:  Correct Correcto" 0 "0:  Incorrect Incorrecto".i ".i: Incomplete Incompleto"
label values recall10 recall10

***10 Word Recall ***
egen D3_t2_correct=rowtotal(recall1- recall10)
replace D3_t2_correct=.i if mc_q31_1_1_16==.i 
label variable D3_t2_correct "D3_t2: Delayed recall of 10 words correct Recuerdo diferido de 10 palabras correcto"
label define D3_t2_correct .i ".i: Incomplete Incompleto"
label values D3_t2_correct D3_t2_correct

egen D3_t2_attempts=rownonmiss(recall1- recall10)
replace D3_t2_attempts=.i if mc_q31_1_1_16==.i
label variable D3_t2_attempts "D3_t2: Delayed recall of 10 words correct Recuerdo diferido de 10 palabras correcto"
label define D3_t2_attempts .i ".i: Incomplete Incompleto"
label values D3_t2_attempts D3_t2_attempts

***Short Story recall attemps***
gen shortstory1_rec=mc_q38_1_16
label variable shortstory1_rec "(1) Short story recall Recuerda cuento corto"
label define shortstory1_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory1_rec shortstory1_rec

gen shortstory2_rec=mc_q38_2_16
label variable shortstory2_rec "(2) Short story recall Recuerda cuento corto"
label define shortstory2_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory2_rec shortstory2_rec

gen shortstory3_rec=mc_q38_3_16
label variable shortstory3_rec "(3) Short story recall Recuerda cuento corto"
label define shortstory3_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory3_rec shortstory3_rec

gen shortstory4_rec=mc_q38_4_16
label variable shortstory4_rec "(4) Short story recall Recuerda cuento corto"
label define shortstory4_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory4_rec shortstory4_rec

gen shortstory5_rec=mc_q38_5_16
label variable shortstory5_rec "(5) Short story recall Recuerda cuento corto"
label define shortstory5_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory5_rec shortstory5_rec

gen shortstory6_rec=mc_q38_6_16
label variable shortstory6_rec "(6) Short story recall Recuerda cuento corto"
label define shortstory6_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory6_rec shortstory6_rec

***Short story recall ***
egen D3_t3_1_correct=rowtotal(shortstory1_rec-shortstory6_rec)
replace D3_t3_1_correct=.i if mc_q38_1_16==.i
replace D3_t3_1_correct=.c if mc_q38_1_16==.c
label variable D3_t3_1_correct "D3_t3: Delayed recall of short story correct Recuerdo diferido de cuento corto correcto"
label define D3_t3_1_correct .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t3_1_correct D3_t3_1_correct

egen D3_t3_1_attempts=rownonmiss(shortstory1_rec-shortstory6_rec)
replace D3_t3_1_attempts = D3_t3_1_attempts*2
replace D3_t3_1_attempts=.i if mc_q38_1_16==.i
replace D3_t3_1_attempts=.c if mc_q38_1_16==.c
label variable D3_t3_1_attempts "D3_t3: Delayed recall of short story attempts Recuerdo diferido de cuento corto intentos"
label define D3_t3_1_attempts .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t3_1_attempts D3_t3_1_attempts

***Short Story recall Aprox Attempt***
gen shortstory_rec_1_a =1 if inlist(mc_q38_1_16,1,2)
replace shortstory_rec_1_a=0 if mc_q38_1_16==0
label variable shortstory_rec_1_a "(1) Approx Short story recall Recuerda cuento corto"
label define shortstory_rec_1_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory_rec_1_a shortstory_rec_1_a

gen shortstory_rec_2_a =1 if inlist(mc_q38_2_16,1,2)
replace shortstory_rec_2_a=0 if mc_q38_2_16==0
label variable shortstory_rec_2_a "(2) Approx Short story recall Recuerda cuento corto"
label define shortstory_rec_2_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory_rec_2_a shortstory_rec_2_a

gen shortstory_rec_3_a =1 if inlist(mc_q38_3_16,1,2)
replace shortstory_rec_3_a=0 if mc_q38_3_16==0
label variable shortstory_rec_3_a "(3) Approx Short story recall Recuerda cuento corto"
label define shortstory_rec_3_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory_rec_3_a shortstory_rec_3_a

gen shortstory_rec_4_a =1 if inlist(mc_q38_4_16,1,2)
replace shortstory_rec_4_a=0 if mc_q38_4_16==0
label variable shortstory_rec_4_a "(4) Approx Short story recall Recuerda cuento corto"
label define shortstory_rec_4_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory_rec_4_a shortstory_rec_4_a

gen shortstory_rec_5_a =1 if inlist(mc_q38_5_16,1,2)
replace shortstory_rec_5_a=0 if mc_q38_5_16==0
label variable shortstory_rec_5_a "(5) Approx Short story recall Recuerda cuento corto"
label define shortstory_rec_5_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory_rec_5_a shortstory_rec_5_a

gen shortstory_rec_6_a =1 if inlist(mc_q38_6_16,1,2)
replace shortstory_rec_6_a=0 if mc_q38_6_16==0
label variable shortstory_rec_6_a "(6) Approx Short story recall Recuerda cuento corto"
label define shortstory_rec_6_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory_rec_6_a shortstory_rec_6_a

***Short Story recall Aprox Score***
egen D3_t3_2 = rowtotal(shortstory_rec_1_a -shortstory_rec_6_a)
replace D3_t3_2=.i if mc_q38_1_16==.i
replace D3_t3_2=.c if mc_q38_1_16==.c
label variable D3_t3_2 "D3_t3_3: Delayed recall of short story approx Recuerdo diferido de cuento corto aproximado"
label define D3_t3_2 .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t3_2 D3_t3_2

***Short Story recall Exact Attempt***
gen shortstory1_rec_e =0 if inlist(mc_q38_1_16,0,1)
replace shortstory1_rec_e=1 if mc_q38_1_16==2
label variable shortstory1_rec_e "(1) Exact Short story recall Recuerda cuento corto"
label define shortstory1_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory1_rec_e shortstory1_rec_e

gen shortstory2_rec_e =0 if inlist(mc_q38_2_16,0,1)
replace shortstory2_rec_e =1 if mc_q38_2_16==2
label variable shortstory2_rec_e "(2) Exact Short story recall Recuerda cuento corto"
label define shortstory2_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory2_rec_e shortstory2_rec_e

gen shortstory3_rec_e =0 if inlist(mc_q38_3_16,0,1)
replace shortstory3_rec_e =1 if mc_q38_3_16==2
label variable shortstory3_rec_e "(3) Exact Short story recall Recuerda cuento corto"
label define shortstory3_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory3_rec_e shortstory3_rec_e

gen shortstory4_rec_e =0 if inlist(mc_q38_4_16,0,1)
replace shortstory4_rec_e =1 if mc_q38_4_16==2
label variable shortstory4_rec_e "(4) Exact Short story recall Recuerda cuento corto"
label define shortstory4_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory4_rec_e shortstory4_rec_e

gen shortstory5_rec_e =0 if inlist(mc_q38_5_16,0,1)
replace shortstory5_rec_e =1 if mc_q38_5_16==2
label variable shortstory5_rec_e "(5) Exact Short story recall Recuerda cuento corto"
label define shortstory5_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory5_rec_e shortstory5_rec_e

gen shortstory6_rec_e =0 if inlist(mc_q38_6_16,0,1)
replace shortstory6_rec_e =1 if mc_q38_6_16==2
label variable shortstory6_rec_e "(6) Exact Short story recall Recuerda cuento corto"
label define shortstory6_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values shortstory6_rec_e shortstory6_rec_e

***Short Story recall Exact Score***
egen D3_t3_3 = rowtotal(shortstory1_rec_e -shortstory6_rec_e)
replace D3_t3_3=.i if mc_q38_1_16==.i
replace D3_t3_3=.c if mc_q38_1_16==.c
label variable D3_t3_3 "D3_t3_4: Delayed recall of short story exact Recuerdo diferido de cuento corto exacto"
label define D3_t3_3 .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t3_3 D3_t3_3

***Long Story recall Attempt***
gen longstory1_rec=mc_q39_1_16
label variable longstory1_rec "(1) Long story recall Recuerda cuento largo"
label define longstory1_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory1_rec longstory1_rec

gen longstory2_rec=mc_q39_2_16
label variable longstory2_rec "(2) Long story recall Recuerda cuento largo"
label define longstory2_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory2_rec longstory2_rec

gen longstory3_rec=mc_q39_3_16
label variable longstory3_rec "(3) Long story recall Recuerda cuento largo"
label define longstory3_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory3_rec longstory3_rec

gen longstory4_rec=mc_q39_4_16
label variable longstory4_rec "(4) Long story recall Recuerda cuento largo"
label define longstory4_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory4_rec longstory4_rec

gen longstory5_rec=mc_q39_5_16
label variable longstory5_rec "(5) Long story recall Recuerda cuento largo"
label define longstory5_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory5_rec longstory5_rec

gen longstory6_rec=mc_q39_6_16
label variable longstory6_rec "(6) Long story recall Recuerda cuento largo"
label define longstory6_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory6_rec longstory6_rec

gen longstory7_rec=mc_q39_7_16
label variable longstory7_rec "(7) Long story recall Recuerda cuento largo"
label define longstory7_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory7_rec longstory7_rec

gen longstory8_rec=mc_q39_8_16
label variable longstory8_rec "(8) Long story recall Recuerda cuento largo"
label define longstory8_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory8_rec longstory8_rec

gen longstory9_rec=mc_q39_9_16
label variable longstory9_rec "(9) Long story recall Recuerda cuento largo"
label define longstory9_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory9_rec longstory9_rec

gen longstory10_rec=mc_q39_10_16
label variable longstory10_rec "(10) Long story recall Recuerda cuento largo"
label define longstory10_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory10_rec longstory10_rec

gen longstory11_rec=mc_q39_11_16
label variable longstory11_rec "(11) Long story recall Recuerda cuento largo"
label define longstory11_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory11_rec longstory11_rec

gen longstory12_rec=mc_q39_12_16
label variable longstory12_rec "(12) Long story recall Recuerda cuento largo"
label define longstory12_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory12_rec longstory12_rec

gen longstory13_rec=mc_q39_13_16
label variable longstory13_rec "(13) Long story recall Recuerda cuento largo"
label define longstory13_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory13_rec longstory13_rec

gen longstory14_rec=mc_q39_14_16
label variable longstory14_rec "(14) Long story recall Recuerda cuento largo"
label define longstory14_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory14_rec longstory14_rec

gen longstory15_rec=mc_q39_15_16
label variable longstory15_rec "(15) Long story recall Recuerda cuento largo"
label define longstory15_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory15_rec longstory15_rec

gen longstory16_rec=mc_q39_16_16
label variable longstory16_rec "(16) Long story recall Recuerda cuento largo"
label define longstory16_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory16_rec longstory16_rec

gen longstory17_rec=mc_q39_17_16
label variable longstory17_rec "(17) Long story recall Recuerda cuento largo"
label define longstory17_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory17_rec longstory17_rec

gen longstory18_rec=mc_q39_18_16
label variable longstory18_rec "(18) Long story recall Recuerda cuento largo"
label define longstory18_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory18_rec longstory18_rec

gen longstory19_rec=mc_q39_19_16
label variable longstory19_rec "(19) Long story recall Recuerda cuento largo"
label define longstory19_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory19_rec longstory19_rec

gen longstory20_rec=mc_q39_20_16
label variable longstory20_rec "(20) Long story recall Recuerda cuento largo"
label define longstory20_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory20_rec longstory20_rec

gen longstory21_rec=mc_q39_21_16
label variable longstory21_rec "(21) Long story recall Recuerda cuento largo"
label define longstory21_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory21_rec longstory21_rec

gen longstory22_rec=mc_q39_22_16
label variable longstory22_rec "(22) Long story recall Recuerda cuento largo"
label define longstory22_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory22_rec longstory22_rec

gen longstory23_rec=mc_q39_23_16
label variable longstory23_rec "(23) Long story recall Recuerda cuento largo"
label define longstory23_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory23_rec longstory23_rec

gen longstory24_rec=mc_q39_24_16
label variable longstory24_rec "(24) Long story recall Recuerda cuento largo"
label define longstory24_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory24_rec longstory24_rec

gen longstory25_rec=mc_q39_25_16
label variable longstory25_rec "(25) Long story recall Recuerda cuento largo"
label define longstory25_rec 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" 2 "2: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory25_rec longstory25_rec

***Long story recall ***
egen D3_t4_1_correct=rowtotal(longstory1_rec-longstory25_rec)
replace D3_t4_1_correct=.i if mc_q39_1_16==.i
replace D3_t4_1_correct=.c if mc_q39_1_16==.c
label variable D3_t4_1_correct "D3_t4_1: Delayed recall of long story correct Recuerdo diferido de cuento largo correcto"
label define D3_t4_1_correct .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t4_1_correct D3_t4_1_correct

egen D3_t4_1_attempts=rownonmiss(longstory1_rec-longstory25_rec)
replace D3_t4_1_attempts = D3_t4_1_attempts*2
replace D3_t4_1_attempts=.i if mc_q39_1_16==.i
replace D3_t4_1_attempts=.c if mc_q39_1_16==.c
label variable D3_t4_1_attempts "D3_t4_1: Delayed recall of long story attempts Recuerdo diferido de cuento largo intentos"
label define D3_t4_1_attempts .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t4_1_attempts D3_t4_1_attempts

***Long Story recall Aprox ***
gen longstory1_rec_a =1 if inlist(mc_q39_1_16,1,2)
replace longstory1_rec_a=0 if mc_q39_1_16==0
label variable longstory1_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory1_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory1_rec_a longstory1_rec_a

gen longstory2_rec_a =1 if inlist(mc_q39_2_16,1,2)
replace longstory2_rec_a=0 if mc_q39_2_16==0
label variable longstory2_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory2_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory2_rec_a longstory2_rec_a

gen longstory3_rec_a =1 if inlist(mc_q39_3_16,1,2)
replace longstory3_rec_a=0 if mc_q39_3_16==0
label variable longstory3_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory3_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory3_rec_a longstory3_rec_a

gen longstory4_rec_a =1 if inlist(mc_q39_4_16,1,2)
replace longstory4_rec_a=0 if mc_q39_4_16==0
label variable longstory4_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory4_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory4_rec_a longstory4_rec_a

gen longstory5_rec_a =1 if inlist(mc_q39_5_16,1,2)
replace longstory5_rec_a=0 if mc_q39_5_16==0
label variable longstory5_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory5_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory5_rec_a longstory5_rec_a

gen longstory6_rec_a =1 if inlist(mc_q39_6_16,1,2)
replace longstory6_rec_a=0 if mc_q39_6_16==0
label variable longstory6_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory6_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory6_rec_a longstory6_rec_a

gen longstory7_rec_a =1 if inlist(mc_q39_7_16,1,2)
replace longstory7_rec_a=0 if mc_q39_7_16==0
label variable longstory7_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory7_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory7_rec_a longstory7_rec_a

gen longstory8_rec_a =1 if inlist(mc_q39_8_16,1,2)
replace longstory8_rec_a=0 if mc_q39_8_16==0
label variable longstory8_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory8_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory8_rec_a longstory8_rec_a

gen longstory9_rec_a =1 if inlist(mc_q39_9_16,1,2)
replace longstory9_rec_a=0 if mc_q39_9_16==0
label variable longstory9_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory9_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory9_rec_a longstory9_rec_a

gen longstory10_rec_a =1 if inlist(mc_q39_10_16,1,2)
replace longstory10_rec_a=0 if mc_q39_10_16==0
label variable longstory10_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory10_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory10_rec_a longstory10_rec_a

gen longstory11_rec_a =1 if inlist(mc_q39_11_16,1,2)
replace longstory11_rec_a=0 if mc_q39_11_16==0
label variable longstory11_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory11_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory11_rec_a longstory11_rec_a

gen longstory12_rec_a =1 if inlist(mc_q39_12_16,1,2)
replace longstory12_rec_a=0 if mc_q39_12_16==0
label variable longstory12_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory12_rec_a 0 "0:  Incorrect Incorrecto" 1 "Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory12_rec_a longstory12_rec_a

gen longstory13_rec_a =1 if inlist(mc_q39_13_16,1,2)
replace longstory13_rec_a=0 if mc_q39_13_16==0
label variable longstory13_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory13_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory13_rec_a longstory13_rec_a

gen longstory14_rec_a =1 if inlist(mc_q39_14_16,1,2)
replace longstory14_rec_a=0 if mc_q39_14_16==0
label variable longstory14_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory14_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory14_rec_a longstory14_rec_a

gen longstory15_rec_a =1 if inlist(mc_q39_15_16,1,2)
replace longstory15_rec_a=0 if mc_q39_15_16==0
label variable longstory15_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory15_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory15_rec_a longstory15_rec_a

gen longstory16_rec_a =1 if inlist(mc_q39_16_16,1,2)
replace longstory16_rec_a=0 if mc_q39_16_16==0
label variable longstory16_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory16_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory16_rec_a longstory16_rec_a

gen longstory17_rec_a =1 if inlist(mc_q39_17_16,1,2)
replace longstory17_rec_a=0 if mc_q39_17_16==0
label variable longstory17_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory17_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory17_rec_a longstory17_rec_a

gen longstory18_rec_a =1 if inlist(mc_q39_18_16,1,2)
replace longstory18_rec_a=0 if mc_q39_18_16==0
label variable longstory18_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory18_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory18_rec_a longstory18_rec_a

gen longstory19_rec_a =1 if inlist(mc_q39_19_16,1,2)
replace longstory19_rec_a=0 if mc_q39_19_16==0
label variable longstory19_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory19_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory19_rec_a longstory19_rec_a

gen longstory20_rec_a =1 if inlist(mc_q39_20_16,1,2)
replace longstory20_rec_a=0 if mc_q39_20_16==0
label variable longstory20_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory20_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory20_rec_a longstory20_rec_a

gen longstory21_rec_a =1 if inlist(mc_q39_21_16,1,2)
replace longstory21_rec_a=0 if mc_q39_21_16==0
label variable longstory21_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory21_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory21_rec_a longstory21_rec_a

gen longstory22_rec_a =1 if inlist(mc_q39_22_16,1,2)
replace longstory22_rec_a=0 if mc_q39_22_16==0
label variable longstory22_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory22_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory22_rec_a longstory22_rec_a

gen longstory23_rec_a =1 if inlist(mc_q39_23_16,1,2)
replace longstory23_rec_a=0 if mc_q39_23_16==0
label variable longstory23_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory23_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory23_rec_a longstory23_rec_a

gen longstory24_rec_a =1 if inlist(mc_q39_24_16,1,2)
replace longstory24_rec_a=0 if mc_q39_24_16==0
label variable longstory24_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory24_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory24_rec_a longstory24_rec_a

gen longstory25_rec_a =1 if inlist(mc_q39_25_16,1,2)
replace longstory25_rec_a=0 if mc_q39_25_16==0
label variable longstory25_rec_a "(1) Approx Long story recall Recuerda cuento largo"
label define longstory25_rec_a 0 "0:  Incorrect Incorrecto" 1 "1: Approximately Aproximadamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory25_rec_a longstory25_rec_a

***Long Story recall Aprox Score***
egen D3_t4_2 = rowtotal(longstory1_rec_a -longstory25_rec_a)
replace D3_t4_2=.i if mc_q39_1_16==.i
replace D3_t4_2=.c if mc_q39_1_16==.c
label variable D3_t4_2 "D3_t4_2: Delayed recall of long story approx Recuerdo diferido de cuento largo aproximado"
label define D3_t4_2 .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t4_2 D3_t4_2

***Long Story recall Exact Attempt***
gen longstory1_rec_e =1 if mc_q39_1_16==2
replace longstory1_rec_e=0 if inlist(mc_q39_1_16,0,1)
label variable longstory1_rec_e "(1) Exact Long story recall Recuerda cuento largo"
label define longstory1_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory1_rec_e longstory1_rec_e

gen longstory2_rec_e =1 if mc_q39_1_16==2
replace longstory2_rec_e=0 if inlist(mc_q39_2_16,0,1)
label variable longstory2_rec_e "(2) Exact Long story recall Recuerda cuento largo"
label define longstory2_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory2_rec_e longstory2_rec_e

gen longstory3_rec_e =1 if mc_q39_1_16==2
replace longstory3_rec_e=0 if inlist(mc_q39_3_16,0,1)
label variable longstory3_rec_e "(3) Exact Long story recall Recuerda cuento largo"
label define longstory3_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory3_rec_e longstory3_rec_e

gen longstory4_rec_e =1 if mc_q39_1_16==2
replace longstory4_rec_e=0 if inlist(mc_q39_4_16,0,1)
label variable longstory4_rec_e "(4) Exact Long story recall Recuerda cuento largo"
label define longstory4_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory4_rec_e longstory4_rec_e

gen longstory5_rec_e =1 if mc_q39_1_16==2
replace longstory5_rec_e=0 if inlist(mc_q39_5_16,0,1)
label variable longstory5_rec_e "(5) Exact Long story recall Recuerda cuento largo"
label define longstory5_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory5_rec_e longstory5_rec_e

gen longstory6_rec_e =1 if mc_q39_1_16==2
replace longstory6_rec_e=0 if inlist(mc_q39_6_16,0,1)
label variable longstory6_rec_e "(6) Exact Long story recall Recuerda cuento largo"
label define longstory6_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory6_rec_e longstory6_rec_e

gen longstory7_rec_e =1 if mc_q39_1_16==2
replace longstory7_rec_e=0 if inlist(mc_q39_7_16,0,1)
label variable longstory7_rec_e "(7) Exact Long story recall Recuerda cuento largo"
label define longstory7_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory7_rec_e longstory7_rec_e

gen longstory8_rec_e =1 if mc_q39_1_16==2
replace longstory8_rec_e=0 if inlist(mc_q39_8_16,0,1)
label variable longstory8_rec_e "(8) Exact Long story recall Recuerda cuento largo"
label define longstory8_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory8_rec_e longstory8_rec_e

gen longstory9_rec_e =1 if mc_q39_1_16==2
replace longstory9_rec_e=0 if inlist(mc_q39_9_16,0,1)
label variable longstory9_rec_e "(9) Exact Long story recall Recuerda cuento largo"
label define longstory9_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory9_rec_e longstory9_rec_e

gen longstory10_rec_e =1 if mc_q39_1_16==2
replace longstory10_rec_e=0 if inlist(mc_q39_10_16,0,1)
label variable longstory10_rec_e "(10) Exact Long story recall Recuerda cuento largo"
label define longstory10_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory10_rec_e longstory10_rec_e

gen longstory11_rec_e =1 if mc_q39_1_16==2
replace longstory11_rec_e=0 if inlist(mc_q39_11_16,0,1)
label variable longstory11_rec_e "(11) Exact Long story recall Recuerda cuento largo"
label define longstory11_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory11_rec_e longstory11_rec_e

gen longstory12_rec_e =1 if mc_q39_1_16==2
replace longstory12_rec_e=0 if inlist(mc_q39_12_16,0,1)
label variable longstory12_rec_e "(12) Exact Long story recall Recuerda cuento largo"
label define longstory12_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory12_rec_e longstory12_rec_e

gen longstory13_rec_e =1 if mc_q39_1_16==2
replace longstory13_rec_e=0 if inlist(mc_q39_13_16,0,1)
label variable longstory13_rec_e "(13) Exact Long story recall Recuerda cuento largo"
label define longstory13_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory13_rec_e longstory13_rec_e

gen longstory14_rec_e =1 if mc_q39_1_16==2
replace longstory14_rec_e=0 if inlist(mc_q39_14_16,0,1)
label variable longstory14_rec_e "(14) Exact Long story recall Recuerda cuento largo"
label define longstory14_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory14_rec_e longstory14_rec_e

gen longstory15_rec_e =1 if mc_q39_1_16==2
replace longstory15_rec_e=0 if inlist(mc_q39_15_16,0,1)
label variable longstory15_rec_e "(15) Exact Long story recall Recuerda cuento largo"
label define longstory15_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory15_rec_e longstory15_rec_e

gen longstory16_rec_e =1 if mc_q39_1_16==2
replace longstory16_rec_e=0 if inlist(mc_q39_16_16,0,1)
label variable longstory16_rec_e "(16) Exact Long story recall Recuerda cuento largo"
label define longstory16_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory16_rec_e longstory16_rec_e

gen longstory17_rec_e =1 if mc_q39_1_16==2
replace longstory17_rec_e=0 if inlist(mc_q39_17_16,0,1)
label variable longstory17_rec_e "(17) Exact Long story recall Recuerda cuento largo"
label define longstory17_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory17_rec_e longstory17_rec_e

gen longstory18_rec_e =1 if mc_q39_1_16==2
replace longstory18_rec_e =0 if inlist(mc_q39_8_16,0,1)
label variable longstory18_rec_e "(18) Exact Long story recall Recuerda cuento largo"
label define longstory18_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory18_rec_e longstory18_rec_e

gen longstory19_rec_e =1 if mc_q39_1_16==2
replace longstory19_rec_e =0 if inlist(mc_q39_19_16,0,1)
label variable longstory19_rec_e "(19) Exact Long story recall Recuerda cuento largo"
label define longstory19_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory19_rec_e longstory19_rec_e

gen longstory20_rec_e =1 if mc_q39_1_16==2
replace longstory20_rec_e=0 if inlist(mc_q39_20_16,0,1)
label variable longstory20_rec_e "(20) Exact Long story recall Recuerda cuento largo"
label define longstory20_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory20_rec_e longstory20_rec_e

gen longstory21_rec_e =1 if mc_q39_1_16==2
replace longstory21_rec_e =0 if inlist(mc_q39_21_16,0,1)
label variable longstory21_rec_e "(21) Exact Long story recall Recuerda cuento largo"
label define longstory21_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory21_rec_e longstory21_rec_e

gen longstory22_rec_e =1 if mc_q39_1_16==2
replace longstory22_rec_e =0 if inlist(mc_q39_22_16,0,1)
label variable longstory22_rec_e "(22) Exact Long story recall Recuerda cuento largo"
label define longstory22_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory22_rec_e longstory22_rec_e

gen longstory23_rec_e =1 if mc_q39_1_16==2
replace longstory23_rec_e=0 if inlist(mc_q39_23_16,0,1)
label variable longstory23_rec_e "(23) Exact Long story recall Recuerda cuento largo"
label define longstory23_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory23_rec_e longstory23_rec_e

gen longstory24_rec_e =1 if mc_q39_1_16==2
replace longstory24_rec_e=0 if inlist(mc_q39_24_16,0,1)
label variable longstory24_rec_e "(24) Exact Long story recall Recuerda cuento largo"
label define longstory24_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory24_rec_e longstory24_rec_e

gen longstory25_rec_e =1 if mc_q39_1_16==2
replace longstory25_rec_e=0 if inlist(mc_q39_25_16,0,1)
label variable longstory25_rec_e "(25) Exact Long story recall Recuerda cuento largo"
label define longstory25_rec_e 0 "0:  Incorrect Incorrecto" 1 "1: Exactly Exactamente" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values longstory25_rec_e longstory25_rec_e

***Long Story recall Exact Score***
egen D3_t4_3 = rowtotal(longstory1_rec_e -longstory25_rec_e)
replace D3_t4_3=.i if mc_q39_1_16==.i
replace D3_t4_3=.c if mc_q39_1_16==.c
label variable D3_t4_3 "D3_t4_3: Delayed recall of long story exact Recuerdo diferido de cuento largo exacto"
label define D3_t4_3 .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t4_3 D3_t4_3

***Word list recognition attempts***
gen wordrec1 = 1 if mc_q34_1_16==2
replace wordrec1 = 0 if inlist(mc_q34_1_16,1,8)
replace wordrec1 = .r if mc_q34_1_16==9
replace wordrec1=.i if mc_q34_1_16==.i
replace wordrec1=.c if mc_q34_1_16==.c
label variable wordrec1 "(1)Word recognition Reconocimiento de palabras"
label define wordrec1 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec1 wordrec1

gen wordrec2 = 1 if mc_q34_2_16==1
replace wordrec2 = 0 if inlist(mc_q34_2_16,2,8)
replace wordrec2 = .r if mc_q34_2_16==9
replace wordrec2=.i if mc_q34_2_16==.i
replace wordrec2=.c if mc_q34_2_16==.c
label variable wordrec2 "(2)Word recognition Reconocimiento de palabras"
label define wordrec2 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec2 wordrec2

gen wordrec3 = 1 if mc_q34_3_16==2
replace wordrec3 = 0 if inlist(mc_q34_3_16,1,8)
replace wordrec3 = .r if mc_q34_3_16==9
replace wordrec3=.i if mc_q34_3_16==.i
replace wordrec3=.c if mc_q34_3_16==.c
label variable wordrec3 "(3)Word recognition Reconocimiento de palabras"
label define wordrec3 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec3 wordrec3

gen wordrec4 = 1 if mc_q34_4_16==1
replace wordrec4 = 0 if inlist(mc_q34_4_16,2,8)
replace wordrec4 = .r if mc_q34_4_16==9
replace wordrec4=.i if mc_q34_4_16==.i
replace wordrec4=.c if mc_q34_4_16==.c
label variable wordrec4 "(4)Word recognition Reconocimiento de palabras"
label define wordrec4 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec4 wordrec4

gen wordrec5 = 1 if mc_q34_5_16==1
replace wordrec5 = 0 if inlist(mc_q34_5_16,2,8)
replace wordrec5 = .r if mc_q34_5_16==9
replace wordrec5=.i if mc_q34_5_16==.i
replace wordrec5=.c if mc_q34_5_16==.c
label variable wordrec5 "(5)Word recognition Reconocimiento de palabras"
label define wordrec5 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec5 wordrec5

gen wordrec6 = 1 if mc_q34_6_16==2
replace wordrec6 = 0 if inlist(mc_q34_6_16,1,8)
replace wordrec6 = .r if mc_q34_6_16==9
replace wordrec6=.i if mc_q34_6_16==.i
replace wordrec6=.c if mc_q34_6_16==.c
label variable wordrec6 "(6)Word recognition Reconocimiento de palabras"
label define wordrec6 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec6 wordrec6

gen wordrec7 = 1 if mc_q34_7_16==2
replace wordrec7 = 0 if inlist(mc_q34_7_16,1,8)
replace wordrec7 = .r if mc_q34_7_16==9
replace wordrec7=.i if mc_q34_7_16==.i
replace wordrec7=.c if mc_q34_7_16==.c
label variable wordrec7 "(7)Word recognition Reconocimiento de palabras"
label define wordrec7 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec7 wordrec7

gen wordrec8 = 1 if mc_q34_8_16==1
replace wordrec8 = 0 if inlist(mc_q34_8_16,2,8)
replace wordrec8 = .r if mc_q34_8_16==9
replace wordrec8=.i if mc_q34_8_16==.i
replace wordrec8=.c if mc_q34_8_16==.c
label variable wordrec8 "(8)Word recognition Reconocimiento de palabras"
label define wordrec8 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec8 wordrec8

gen wordrec9 = 1 if mc_q34_9_16==1
replace wordrec9 = 0 if inlist(mc_q34_9_16,2,8)
replace wordrec9 = .r if mc_q34_9_16==9
replace wordrec9=.i if mc_q34_9_16==.i
replace wordrec9=.c if mc_q34_9_16==.c
label variable wordrec9 "(9)Word recognition Reconocimiento de palabras"
label define wordrec9 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec9 wordrec9

gen wordrec10 = 1 if mc_q34_10_16==2
replace wordrec10 = 0 if inlist(mc_q34_10_16,1,8)
replace wordrec10 = .r if mc_q34_10_16==9
replace wordrec10=.i if mc_q34_10_16==.i
replace wordrec10=.c if mc_q34_10_16==.c
label variable wordrec10 "(10)Word recognition Reconocimiento de palabras"
label define wordrec10 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec10 wordrec10

gen wordrec11 = 1 if mc_q34_11_16==1
replace wordrec11 = 0 if inlist(mc_q34_11_16,2,8)
replace wordrec11 = .r if mc_q34_11_16==9
replace wordrec11=.i if mc_q34_11_16==.i
replace wordrec11=.c if mc_q34_11_16==.c
label variable wordrec11 "(11)Word recognition Reconocimiento de palabras"
label define wordrec11 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec11 wordrec11

gen wordrec12 = 1 if mc_q34_12_16==2
replace wordrec12 = 0 if inlist(mc_q34_12_16,1,8)
replace wordrec12 = .r if mc_q34_12_16==9
replace wordrec12=.i if mc_q34_12_16==.i
replace wordrec12=.c if mc_q34_12_16==.c
label variable wordrec12 "(12)Word recognition Reconocimiento de palabras"
label define wordrec12 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec12 wordrec12

gen wordrec13 = 1 if mc_q34_13_16==2
replace wordrec13 = 0 if inlist(mc_q34_13_16,1,8)
replace wordrec13 = .r if mc_q34_13_16==9
replace wordrec13=.i if mc_q34_13_16==.i
replace wordrec13=.c if mc_q34_13_16==.c
label variable wordrec13 "(13)Word recognition Reconocimiento de palabras"
label define wordrec13 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec13 wordrec13

gen wordrec14 = 1 if mc_q34_14_16==1
replace wordrec14 = 0 if inlist(mc_q34_14_16,2,8)
replace wordrec14 = .r if mc_q34_14_16==9
replace wordrec14=.i if mc_q34_14_16==.i
replace wordrec14=.c if mc_q34_14_16==.c
label variable wordrec14 "(14)Word recognition Reconocimiento de palabras"
label define wordrec14 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec14 wordrec1

gen wordrec15 = 1 if mc_q34_15_16==1
replace wordrec15 = 0 if inlist(mc_q34_15_16,2,8)
replace wordrec15 = .r if mc_q34_15_16==9
replace wordrec15=.i if mc_q34_15_16==.i
replace wordrec15=.c if mc_q34_15_16==.c
label variable wordrec15 "(15)Word recognition Reconocimiento de palabras"
label define wordrec15 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec15 wordrec15

gen wordrec16 = 1 if mc_q34_16_16==2
replace wordrec16 = 0 if inlist(mc_q34_16_16,1,8)
replace wordrec16 = .r if mc_q34_16_16==9
replace wordrec16=.i if mc_q34_16_16==.i
replace wordrec16=.c if mc_q34_16_16==.c
label variable wordrec16 "(16)Word recognition Reconocimiento de palabras"
label define wordrec16 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec16 wordrec1

gen wordrec17 = 1 if mc_q34_17_16==2
replace wordrec17 = 0 if inlist(mc_q34_17_16,1,8)
replace wordrec17 = .r if mc_q34_17_16==9
replace wordrec17=.i if mc_q34_17_16==.i
replace wordrec17=.c if mc_q34_17_16==.c
label variable wordrec17 "(17)Word recognition Reconocimiento de palabras"
label define wordrec17 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec17 wordrec1

gen wordrec18 = 1 if mc_q34_18_16==1
replace wordrec18 = 0 if inlist(mc_q34_18_16,2,8)
replace wordrec18 = .r if mc_q34_18_16==9
replace wordrec18=.i if mc_q34_18_16==.i
replace wordrec18=.c if mc_q34_18_16==.c
label variable wordrec18 "(18)Word recognition Reconocimiento de palabras"
label define wordrec18 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec18 wordrec1

gen wordrec19 = 1 if mc_q34_19_16==2
replace wordrec19 = 0 if inlist(mc_q34_19_16,1,8)
replace wordrec19 = .r if mc_q34_19_16==9
replace wordrec19=.i if mc_q34_19_16==.i
replace wordrec19=.c if mc_q34_19_16==.c
label variable wordrec19 "(19)Word recognition Reconocimiento de palabras"
label define wordrec19 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec19 wordrec1

gen wordrec20 = 1 if mc_q34_20_16==1
replace wordrec20 = 0 if inlist(mc_q34_20_16,2,8)
replace wordrec20 = .r if mc_q34_20_16==9
replace wordrec20=.i if mc_q34_20_16==.i
replace wordrec20=.c if mc_q34_20_16==.c
label variable wordrec20 "(20)Word recognition Reconocimiento de palabras"
label define wordrec20 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values wordrec20 wordrec20

***Word list recognition***
egen D3_t5_correct=rowtotal(wordrec1-wordrec20)
replace D3_t5_correct=.i if mc_q34_1_16==.i 
replace D3_t5_correct=.c if mc_q34_1_16==.c 
replace D3_t5_correct=.r if wordrec1==.r & wordrec2==.r & wordrec3==.r & wordrec4==.r & wordrec5==.r & wordrec6==.r & wordrec7==.r & wordrec8==.r & wordrec9==.r & wordrec10==.r & wordrec11==.r & wordrec12==.r & wordrec13==.r & wordrec14==.r & wordrec15==.r & wordrec16==.r & wordrec17==.r & wordrec18==.r & wordrec19==.r & wordrec20==.r
label variable D3_t5_correct "Recall by recognition 10 words correct Recuerdo por reconocimiento 10 palabras correcto"
label define D3_t5_correct .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t5_correct D3_t5_correct

egen D3_t5_attempts=rownonmiss(wordrec1-wordrec20)
replace D3_t5_attempts=.i if mc_q34_1_16==.i 
replace D3_t5_attempts=.c if mc_q34_1_16==.c 
replace D3_t5_attempts=.r if wordrec1==.r & wordrec2==.r & wordrec3==.r & wordrec4==.r & wordrec5==.r & wordrec6==.r & wordrec7==.r & wordrec8==.r & wordrec9==.r & wordrec10==.r & wordrec11==.r & wordrec12==.r & wordrec13==.r & wordrec14==.r & wordrec15==.r & wordrec16==.r & wordrec17==.r & wordrec18==.r & wordrec19==.r & wordrec20==.r
label variable D3_t5_attempts "Recall by recognition 10 words attempts Recuerdo por reconocimiento 10 palabras intentos"
label define D3_t5_attempts .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values D3_t5_attempts D3_t5_attempts

***Constructional praxis recall Circle***
gen circle_rec=mc_q37_1_16
replace circle_rec =.r if mc_q37_1_16==99
label variable circle_rec "Recalls figure circle Recuerda figura circulo"
label define circle_rec .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values circle_rec circle_rec

***Constructional praxis recall Diamond***
gen diamond_rec=mc_q37_2_16
replace diamond_rec =.r if mc_q37_2_16==99
label variable diamond_rec "Recalls figure diamond Recuerda figura diamante"
label define diamond_rec .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values diamond_rec diamond_rec

***Constructional praxis recall Rectangles***
gen rectangle_rec=mc_q37_3_16
replace rectangle_rec =.r if mc_q37_3_16==99
label variable rectangle_rec "Recalls figure rectangles Recuerda figura rectangulos"
label define rectangle_rec .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values rectangle_rec rectangle_rec

***Constructional praxis recall Cube***
gen cube_rec=mc_q37_4_16
replace cube_rec =.r if mc_q37_4_16==99
label variable cube_rec "Recalls figure cube Recuerda figura cubo"
label define cube_rec .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values cube_rec cube_rec

***Recall 4 figures***
egen D3_t6_correct=rowtotal(circle_rec diamond_rec rectangle_rec cube_rec)
replace D3_t6_correct=.i if circle_rec==.i & diamond_rec==.i & rectangle_rec==.i & cube_rec==.i
replace D3_t6_correct=.l if circle_rec==.l & diamond_rec==.l & rectangle_rec==.l & cube_rec==.l
replace D3_t6_correct=.c if circle_rec==.c & diamond_rec==.c & rectangle_rec==.c & cube_rec==.c
replace D3_t6_correct=.r if circle_rec==.r & diamond_rec==.r  & rectangle_rec==.r & cube_rec==.r
label variable D3_t6_correct "D3_T6: Delayed four figures correct Recuerdo diferido de cuatro figuras correctos"
label define D3_t6_correct .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D3_t6_correct D3_t6_correct

***Dummy variables for points attempted***
gen circlepointrec=2 if inlist(circle_rec,0,1,2)
gen diamondpointrec=3 if inlist(diamond_rec,0,1,2,3)
gen rectanglepointrec=2 if inlist(rectangle_rec,0,1,2)
gen cubepointrec=4 if inlist(cube_rec,0,1,2,3,4)

egen D3_t6_attempts=rowtotal(circlepointrec diamondpointrec rectanglepointrec cubepointrec)
replace D3_t6_attempts=.i if circle_rec==.i & diamond_rec==.i & rectangle_rec==.i & cube_rec==.i
replace D3_t6_attempts=.l if circle_rec==.l & diamond_rec==.l & rectangle_rec==.l & cube_rec==.l
replace D3_t6_attempts=.c if circle_rec==.c & diamond_rec==.c & rectangle_rec==.c & cube_rec==.c
replace D3_t6_attempts=.r if circle_rec==.r & diamond_rec==.r  & rectangle_rec==.r & cube_rec==.r
label variable D3_t6_attempts "D3_T6: Delayed four figures attempt Recuerdo diferido de cuatro figuras intentos"
label define D3_t6_attempts .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D3_t6_attempts D3_t6_attempts

				**********************************
				***Delayed Memory domain score****
				**********************************
			 
egen D3_DelayedMemory_correct=rowtotal(D3_t1_correct D3_t2_correct D3_t3_1_correct D3_t4_1_correct D3_t5_correct D3_t6_correct)
replace D3_DelayedMemory_correct=.i if D3_t1_correct==.i & D3_t2_correct==.i & D3_t3_1_correct==.i & D3_t4_1_correct ==.i & D3_t5_correct==.i & D3_t6_correct==.i
replace D3_DelayedMemory_correct=.l if D3_t1_correct==.l & D3_t2_correct==.l & D3_t3_1_correct==.l & D3_t4_1_correct ==.l & D3_t5_correct==.l & D3_t6_correct==.l
replace D3_DelayedMemory_correct=.c if D3_t1_correct==.c & D3_t2_correct==.c & D3_t3_1_correct==.c & D3_t4_1_correct ==.c & D3_t5_correct==.c & D3_t6_correct==.c
replace D3_DelayedMemory_correct=.r if D3_t1_correct==.r & D3_t2_correct==.r & D3_t3_1_correct==.r & D3_t4_1_correct ==.r & D3_t5_correct==.r & D3_t6_correct==.r
label variable D3_DelayedMemory_correct "D3: Delayed memory correct Memoria diferida correcto"
label define D3_DelayedMemory_correct .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D3_DelayedMemory_correct D3_DelayedMemory_correct


egen D3_DelayedMemory_attempts=rowtotal(D3_t1_attempts D3_t2_attempts D3_t3_1_attempts D3_t4_1_attempts D3_t5_attempts D3_t6_attempts)
replace D3_DelayedMemory_attempts=.i if D3_t1_attempts==.i & D3_t2_attempts==.i & D3_t3_1_attempts==.i & D3_t4_1_attempts ==.i & D3_t5_attempts==.i & D3_t6_attempts==.i
replace D3_DelayedMemory_attempts=.l if D3_t1_attempts==.l & D3_t2_attempts==.l & D3_t3_1_attempts==.l & D3_t4_1_attempts ==.l & D3_t5_attempts==.l & D3_t6_attempts==.l
replace D3_DelayedMemory_attempts=.c if D3_t1_attempts==.c & D3_t2_attempts==.c & D3_t3_1_attempts==.c & D3_t4_1_attempts ==.c & D3_t5_attempts==.c & D3_t6_attempts==.c
replace D3_DelayedMemory_attempts=.r if D3_t1_attempts==.r & D3_t2_attempts==.r & D3_t3_1_attempts==.r & D3_t4_1_attempts ==.r & D3_t5_attempts==.r & D3_t6_attempts==.r
label variable D3_DelayedMemory_attempts "D3: Delayed memory attempts Memoria diferida intentos"
label define D3_DelayedMemory_attempts .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D3_DelayedMemory_attempts D3_DelayedMemory_attempts

***********************
****ATTENTION DOMAIN***
***********************

***Visual Scan***
gen D4_t1_correct = mc_q22_1_16
replace D4_t1_correct = .r if mc_q22_1_16==99
replace D4_t1_correct=.l if mc_q10_1_l_16==1|mc_q11_1_l_16==1
replace D4_t1_correct=.i if mc_q22_1_16==.i
label variable D4_t1_correct "Visual scan correct Deteccion visual correcto"
label define D4_t1_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D4_t1_correct D4_t1_correct

gen D4_t1_incorrect = mc_q22_2_16
replace D4_t1_incorrect = .r if mc_q22_2_16==99
replace D4_t1_incorrect=.l if mc_q22_2_16==1|mc_q11_1_l_16==1
replace D4_t1_incorrect=.i if mc_q22_2_16==.i
label variable D4_t1_incorrect "Visual scan incorrect Deteccion visual incorrecto"
label define D4_t1_incorrect .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D4_t1_incorrect D4_t1_incorrect

***Numeracy ***
gen numeracy=1 if mc_q23_1_16==1
replace numeracy=1 if mc_q23_2_16==1
replace numeracy=0 if mc_q23_1_16==2
replace numeracy=0 if mc_q23_2_16==2
replace numeracy=.r if mc_q23_1_16==9
replace numeracy=.r if mc_q23_2_16==9
replace numeracy=.i if mc_q23_1_16==.i
label variable numeracy "Backward countdown completed Cuentra regresiva realizada"
label define numeracy 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto".r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values numeracy numeracy

gen numeracy_time = 1 if inrange(mc_q24_16, 31,59)
replace numeracy_time = 2 if inrange(mc_q24_16, 21,30)
replace numeracy_time = 3 if inrange(mc_q24_16, 11,20)
replace numeracy_time = 4 if inrange(mc_q24_16, 2,10)
replace numeracy_time = 0 if numeracy==0
replace numeracy_time = .r if numeracy==.r
replace numeracy_time = .i if numeracy==.i

gen D4_t2=numeracy+numeracy_time
replace D4_t2=1 if numeracy==1 & mc_q24_16==60
replace D4_t2 = .r if numeracy==.r
replace D4_t2 = .i if numeracy==.i
label variable D4_t2 "Backwards counting Cuentra regresiva"
label define D4_t2 0 "0:  Incorrect Incorrecto" 1 "1: More than 60 seconds Mas de 60 segundos" 2 "2: 31-59 seconds" 3 "3: 21-30 seconds" 4 "4: 11-20 seconds" 5 "5: 3-10 seconds" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D4_t2 D4_t2

				****************************
                ***Attention domain score***
                ****************************
				 

egen D4_Attention_correct=rowtotal(D4_t1_correct D4_t2)
replace D4_Attention_correct = .i if D4_t1_correct==.i & D4_t2==.i
replace D4_Attention_correct = .r if D4_t1_correct==.r & D4_t2==.r
replace D4_Attention_correct = .r if D4_t1_correct==.l & D4_t2==.r
label variable D4_Attention_correct "D4: Attention correct Atencion correctos"
label define D4_Attention_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D4_Attention_correct D4_Attention_correct

***Dummy variables for visual scan***
gen numeracypoint=5 if inrange(D4_t2,0,5)
gen visualscanpoint=60 if inrange(D4_t1_correct,0,60)

egen D4_Attention_attempts=rowtotal(numeracypoint visualscanpoint)
replace D4_Attention_attempts = .i if D4_t1_correct==.i & D4_t2==.i
replace D4_Attention_attempts = .r if D4_t1_correct==.r & D4_t2==.r
replace D4_Attention_attempts = .r if D4_t1_correct==.l & D4_t2==.r
label variable D4_Attention_attempts "D4: Attention attempts Atencion intentos"
label define D4_Attention_attempts .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D4_Attention_attempts D4_Attention_attempts

*********************
***LANGUAGE DOMAIN***
*********************

***Instruction paper***
gen paper1=1 if mc_q11_1_16==1
replace paper1=0 if inlist(mc_q11_1_16,2,8)
replace paper1=.r if inlist(mc_q11_1_16,9)
replace paper1=.l if inlist(mc_q10_1_l_16,1)
replace paper1=.i if inlist(mc_q11_1_16,.i)
label variable paper1 "Instruction Take paper Instrucciones tomar papel"
label define paper1 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto".r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values paper1 paper1

 ***Instructions fold***
gen paper2=1 if mc_q11_2_16==1
replace paper2=0 if inlist(mc_q11_2_16,2,8)
replace paper2=.r if inlist(mc_q11_2_16,9)
replace paper2=.l if inlist(mc_q10_1_l_16,1)
replace paper2=.i if inlist(mc_q11_2_16,.i)
label variable paper2 "Instruction fold paper Instrucciones doblar papel"
label define paper2 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto".r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values paper2 paper2

***Instructions floor***
gen paper3=1 if mc_q11_3_16==1
replace paper3=0 if inlist(mc_q11_3_16,2,8)
replace paper3=.r if inlist(mc_q11_3_16,9)
replace paper3=.l if inlist(mc_q10_1_l_16,1)
replace paper3=.i if inlist(mc_q11_3_16,.i)
label variable paper3 "Instruction leave paper Instrucciones dejar papel"
label define paper3 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto".r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values paper3 paper3

***Instructions paper****
egen D5_t1_correct=rowtotal(paper1 paper2 paper3)
replace D5_t1_correct=.r if paper1==.r & paper2==.r & paper3==.r
replace D5_t1_correct=.i if paper1==.i 
replace D5_t1_correct=.l if paper1==.l 
label variable D5_t1_correct "D5_t1: Following isntructions 3 steps correct Seguimiento de instrucciones 3 pasos correctos"
label define D5_t1_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D5_t1_correct D5_t1_correct

egen D5_t1_attempts=rownonmiss(paper1 paper2 paper3)
replace D5_t1_attempts=.r if paper1==.r & paper2==.r & paper3==.r
replace D5_t1_attempts=.i if paper1==.i 
replace D5_t1_attempts=.l if paper1==.l
label variable D5_t1_attempts "D5_t1: Following isntructions 3 steps attempts Seguimiento de instrucciones 3 pasos sintentos"
label define D5_t1_attempts .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D5_t1_attempts D5_t1_attempts

***Point to floor***
gen floor=1 if mc_q30_1_16==1
replace floor=0 if inlist(mc_q30_1_16,2,8)
replace floor=.r if mc_q30_1_16==9
replace floor=.i if mc_q30_1_16==.i
label variable floor "Point floor Seniala suelo"
label define floor 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto".r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values floor floor

***Point to sky***
gen sky=1 if mc_q30_2_16==1
replace sky=0 if inlist(mc_q30_2_16,2,8)
replace sky=.r if mc_q30_2_16==9
replace sky=.i if mc_q30_2_16==.i
label variable sky "D5_t2: Following isntructions 3 steps correct Seguimiento de instrucciones 3 pasos correctos"
label define sky 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto".r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values sky sky

***Point to sky floor***
egen D5_t2_correct=rowtotal(floor sky)
replace D5_t2_correct=.r if floor==.r & sky==.r
replace D5_t2_correct=.i if floor==.i 
label variable D5_t2_correct "D5_t2: Following isntructions 2 steps Correct Seguimiento de instrucciones 2 pasos correcto"
label define D5_t2_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D5_t2_correct D5_t2_correct

egen D5_t2_attempts=rownonmiss(floor sky)
replace D5_t2_attempts=.r if floor==.r & sky==.r
replace D5_t2_attempts=.i if floor==.i  
label variable D5_t2_attempts "D5_t2: Following isntructions 2 steps attempts Seguimiento de instrucciones 2 pasos intentos"
label define D5_t2_attempts .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D5_t2_attempts D5_t2_attempts

***Naming shoe***
gen D5_t3=1 if mc_q12_16==1
replace D5_t3=0 if inlist(mc_q12_16,2,8)
replace D5_t3=.r if inlist(mc_q12_16,9)
replace D5_t3=.l if inlist(mc_q11_1_l_16,1)
replace D5_t3=.i if inlist(mc_q12_16,.i)
label variable D5_t3 "D5_t3: Naming shoe Denominar zapato"
label define D5_t3 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D5_t3 D5_t3

***Naming pencil***
gen D5_t4=1 if mc_q13_16==1
replace D5_t4=0 if inlist(mc_q13_16,2,8)
replace D5_t4=.r if inlist(mc_q13_16,9)
replace D5_t4=.l if inlist(mc_q11_1_l_16,1)
replace D5_t4=.i if inlist(mc_q13_16,.i)
label variable D5_t4 "D5_t4: Naming pencil Denominar lapiz"
label define D5_t4 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D5_t4 D5_t4

***Elbows***
gen D5_t5=1 if mc_q25_16==1
replace D5_t5=0 if inlist(mc_q25_16,2,8)
replace D5_t5=.r if mc_q25_16==9
replace D5_t5=.i if mc_q25_16==.i
replace D5_t5=.l if mc_q25_16==.l
replace D5_t5=.m if mc_q25_16==.m
label variable D5_t5 "D5_t5: Naming elbows Denominar codos"
label define D5_t5 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica" .m ".m: Missing Perdidos"
label values D5_t5 D5_t5

***Birdge***
gen D5_t6=1 if mc_q26_16==1
replace D5_t6=0 if inlist(mc_q26_16,2,8)
replace D5_t6=.r if mc_q26_16==9
replace D5_t6=.i if mc_q26_16==.i
label variable D5_t6 "D5_t6 Define bridge Definir puente"
label define D5_t6 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D5_t6 D5_t6

***Hammer***
gen D5_t7=1 if mc_q27_16==1
replace D5_t7=0 if inlist(mc_q27_16,2,8)
replace D5_t7=.r if mc_q27_16==9
replace D5_t7=.i if mc_q27_16==.i
label variable D5_t7 "D5_t7: Use of hammer Uso del martillo"
label define D5_t7 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D5_t7 D5_t7

***Scissors***
gen D5_t8=1 if mc_q28_16==1
replace D5_t8=0 if inlist(mc_q28_16,2,8)
replace D5_t8=.r if mc_q28_16==9
replace D5_t8=.i if mc_q28_16==.i
label variable D5_t8 "D5_t8: Use of scissors Uso de tijeras"
label define D5_t8 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D5_t8 D5_t8

***Repeat phrase "Better late than never"***
gen D5_t9=1 if mc_q14_16==1
replace D5_t9=0 if inlist(mc_q14_16,2,8)
replace D5_t9=.r if inlist(mc_q14_16,9)
replace D5_t9=.i if inlist(mc_q14_16,.i)
label variable D5_t9 "D5_t9: Repetition Repeticion"
label define D5_t9 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D5_t9 D5_t9

***Read "Close your eyes"***
gen D5_t10=1 if mc_q15_16==1
replace D5_t10=0 if inlist(mc_q15_16,2,8)
replace D5_t10=.r if inlist(mc_q15_16,9)
replace D5_t10=.l if inlist(mc_q11_1_l_16,1)
replace D5_t10=.s if inlist(mc_q15_16,3)
replace D5_t10=.i if inlist(mc_q15_16,.i)
label variable D5_t10 "D5_t10: Reading Lectura"
label define D5_t10 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  .l ".l: Physical limitation Limitacion fisica" .s "Skip Salto"
label values D5_t10 D5_t10

***Write a sentece***
gen D5_t11=1 if mc_q16_16==1
replace D5_t11=0 if inlist(mc_q16_16,0,8)
replace D5_t11 =.r if mc_q16_16==9
replace D5_t11=.s if mc_q16_16==3
recode D5_t11(.=.l) if mc_q10_1_l_16==1|mc_q11_1_l_16==1
replace D5_t11=.l if inlist(mc_q16_16,4,5)
replace D5_t11=.i if inlist(mc_q16_16,.i)
label variable D5_t11 "D5_t11: Writing Escritura"
label define D5_t11 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  .l ".l: Physical limitation Limitacion fisica" .s "Skip Salto"
label values D5_t11 D5_t11


				****************************
                ***Language domain score***
                ****************************
				 
egen D5_Language_correct=rowtotal(D5_t1_correct D5_t2_correct D5_t3 D5_t4 D5_t5 D5_t6 D5_t7 D5_t8 D5_t9 D5_t10 D5_t11)
replace D5_Language_correct = .i if D5_t1_correct==.i & D5_t2_correct==.i & D5_t3==.i & D5_t4==.i & D5_t5==.i & D5_t6==.i & D5_t7==.i & D5_t8==.i & D5_t9==.i & D5_t10==.i & D5_t11==.i
replace D5_Language_correct = .r if D5_t1_correct==.r & D5_t2_correct==.r & D5_t3==.r & D5_t4==.r & D5_t5==.r & D5_t6==.r & D5_t7==.r & D5_t8==.r & D5_t9==.r & D5_t10==.r & D5_t11== .r
label variable D5_Language_correct "D5: Language correct Lenguaje correctos"
label define D5_Language_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D5_Language_correct D5_Language_correct

egen D5_Language_attempts=rownonmiss(D5_t1_correct D5_t2_correct D5_t3 D5_t4 D5_t5 D5_t6 D5_t7 D5_t8 D5_t9 D5_t10 D5_t11)
replace D5_Language_attempts = .i if D5_t1_correct==.i & D5_t2_correct==.i & D5_t3==.i & D5_t4==.i & D5_t5==.i & D5_t6==.i & D5_t7==.i & D5_t8==.i & D5_t9==.i & D5_t10==.i & D5_t11==.i
replace D5_Language_attempts = .r if D5_t1_correct==.r & D5_t2_correct==.r & D5_t3==.r & D5_t4==.r & D5_t5==.r & D5_t6==.r & D5_t7==.r & D5_t8==.r & D5_t9==.r & D5_t10==.r & D5_t11== .r
label variable D5_Language_attempts "D5: Language attempts Lenguaje intentos"
label define D5_Language_attempts .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D5_Language_attempts D5_Language_attempts


***Praxis Pentagon***
gen D6_t1=1 if mc_q17_16==1
replace D6_t1=0 if inlist(mc_q17_16, 0,2,8)
replace D6_t1 =.r if mc_n17_16==9
replace D6_t1 =.l if mc_n17_16==.l
recode D6_t1(.=.l) if mc_q11_1_l_16==1
recode D6_t1(.=.l) if mc_q10_1_l_16==1
recode D6_t1(.=.l) if inlist(mc_q16_16,4,5)
replace D6_t1=.i if mc_q17_16==.i
label variable D6_t1 "D6_t1: Copy one figure Copia una figura"
label define D6_t1 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  .l ".l: Physical limitation Limitacion fisica" 
label values D6_t1 D6_t1


***Constructional praxis Circle***
gen circle=mc_q35_1_16 if inlist(mc_q35_1_16,0,1,2)
replace circle =.s if mc_q35_1_16==.s
replace circle =.r if mc_q35_1_16==99
replace circle=.c if mc_q35_1_16==.c
replace circle=.l if mc_q35_1_16==.l
replace circle=.i if mc_q35_1_16==.i
label variable circle "Copies figure circle Copia figura circulo"
label define circle 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c "Short intervire Entrevista corta" .i ".i: Incomplete Incompleto"  .l ".l: Physical limitation Limitacion fisica" 
label values circle circle

***Constructional praxis Diamond***
gen diamond=mc_q35_2_16 if inlist(mc_q35_2_16,0,1,2,3)
replace diamond =.s if mc_q35_2_16==.s
replace diamond =.r if mc_q35_2_16==99
replace diamond=.c if mc_q35_2_16==.c
replace diamond=.l if mc_q35_2_16==.l
replace diamond=.i if mc_q35_2_16==.i
label variable diamond "Copies figure diamond Copia figura diamente"
label define diamond 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c "Short intervire Entrevista corta" .i ".i: Incomplete Incompleto"  .l ".l: Physical limitation Limitacion fisica" 
label values diamond diamond

***Constructional praxis Rectangles***
gen rectangle=mc_q35_3_16 if inlist(mc_q35_3_16,0,1,2)
replace rectangle =.s if mc_q35_3_16==.s
replace rectangle =.r if mc_q35_3_16==99
replace rectangle=.c if mc_q35_3_16==.c
replace rectangle=.l if mc_q35_3_16==.l
replace rectangle=.i if mc_q35_3_16==.i
label variable rectangle "Copies figure rectangles Copia figura rectangulos"
label define rectangle 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c "Short intervire Entrevista corta" .i ".i: Incomplete Incompleto"  .l ".l: Physical limitation Limitacion fisica" 
label values rectangle rectangle

***Constructional praxis Cube***
gen cube=mc_q35_4_16 if inlist(mc_q35_4_16,0,1,2,3,4,99)
replace cube =.s if mc_q35_4_16==.s
replace cube =.r if mc_q35_4_16==99
replace cube=.c if mc_q35_4_16==.c
replace cube=.l if mc_q35_4_16==.l
replace cube=.i if mc_q35_4_16==.i
label variable cube "Copies figure cube Copia figura cubo"
label define cube 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c "Short intervire Entrevista corta" .i ".i: Incomplete Incompleto"  .l ".l: Physical limitation Limitacion fisica" 
label values cube cube

***Figures***
egen D6_t2_correct=rowtotal(circle diamond rectangle cube)
replace D6_t2_correct=.i if circle==.i & diamond==.i & rectangle==.i & cube==.i
replace D6_t2_correct=.l if circle==.l & diamond==.l & rectangle==.l & cube==.l
replace D6_t2_correct=.c if circle==.c & diamond==.c & rectangle==.c & cube==.c
replace D6_t2_correct=.r if circle==.r & diamond==.r  & rectangle==.r & cube==.r
label variable D6_t2_correct "Copies four figures correct Copia cuatro figuras correctos"
label define D6_t2_correct .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D6_t2_correct D6_t2_correct

***Dummy variables for figure attempts***
gen circlepoint=2 if inlist(circle,0,1,2)
gen diamondpoint=3 if inlist(diamond,0,1,2,3)
gen rectanglepoint=2 if inlist(rectangle,0,1,2)
gen cubepoint=4 if inlist(cube,0,1,2,3,4)

egen D6_t2_attempts=rowtotal(circlepoint diamondpoint rectanglepoint cubepoint)
replace D6_t2_attempts=.i if circle==.i & diamond==.i & rectangle==.i & cube==.i
replace D6_t2_attempts=.l if circle==.l & diamond==.l & rectangle==.l & cube==.l
replace D6_t2_attempts=.c if circle==.c & diamond==.c & rectangle==.c & cube==.c
replace D6_t2_attempts=.r if circle==.r & diamond==.r  & rectangle==.r & cube==.r
label variable D6_t2_attempts "Copies four figures attempts Copia cuatro figuras intentos"
label define D6_t2_attempts .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D6_t2_attempts D6_t2_attempts

				**********************************
				***Constructional praxis domain***
				**********************************
egen D6_ConstructionalPraxis_correct=rowtotal(D6_t1 D6_t2_correct)
replace D6_ConstructionalPraxis_correct=.i if D6_t1==.i & D6_t2_correct==.i 
replace D6_ConstructionalPraxis_correct=.r if D6_t1==.r & D6_t2_correct==.r
replace D6_ConstructionalPraxis_correct=.c if D6_t1==.c & D6_t2_correct==.c
replace D6_ConstructionalPraxis_correct=.l if D6_t1==.l & D6_t2_correct==.l
label variable D6_ConstructionalPraxis_correct "D6: Constructional Praxis correct Praxias Contrucctionales correcto "
label define D6_ConstructionalPraxis_correct .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D6_ConstructionalPraxis_correct D6_ConstructionalPraxis_correct

***Dummy variable pentagon point***
gen pentagonspoint=1 if inlist(D6_t1,0,1)

egen D6_ConstructionalPraxis_attempts=rowtotal(pentagonspoint D6_t2_attempts)
replace D6_ConstructionalPraxis_attempts=.i if D6_t1==.i & D6_t2_attempts==.i 
replace D6_ConstructionalPraxis_attempts=.r if D6_t1==.r & D6_t2_attempts==.r
replace D6_ConstructionalPraxis_attempts=.c if D6_t1==.c & D6_t2_attempts==.c
replace D6_ConstructionalPraxis_attempts=.l if D6_t1==.l & D6_t2_correct==.l
label variable D6_ConstructionalPraxis_attempts "D6: Constructional Praxis attempts Praxias Contrucctionales intentos"
label define D6_ConstructionalPraxis_attempts .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D6_ConstructionalPraxis_attempts D6_ConstructionalPraxis_attempts

***SERIAL3 1ST Score***
gen ser3_1=1 if inlist(mc_q8_1_16,17,20)
replace ser3_1=0 if mc_q8_1_16!=17 & mc_q8_1_16!=20
replace ser3_1=.i if mc_q8_1_16==.i
replace ser3_1=.r if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
label variable ser3_1 "Serial 3 Round1 Resta de 3 Ronda1"
label define ser3_1 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser3_1 ser3_1

***SERIAL3 2ND Score***
gen ser3_2=1 if mc_q8_1_16-mc_q8_2_16==3
replace ser3_2=0 if mc_q8_1_16-mc_q8_2_16!=3
replace ser3_2=.i if mc_q8_2_16==.i
replace ser3_2=.r if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
label variable ser3_2 "Serial 3 Round2 Resta de 3 Ronda2"
label define ser3_2 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser3_2 ser3_2

***SERIAL3 3RD Score***
gen ser3_3=1 if mc_q8_2_16-mc_q8_3_16==3
replace ser3_3=0 if mc_q8_2_16-mc_q8_3_16!=3
replace ser3_3=.i if mc_q8_3_16==.i
replace ser3_3=.r if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
label variable ser3_3 "Serial 3 Round3 Resta de 3 Ronda3"
label define ser3_3 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser3_3 ser3_3

***SERIAL3 4TH Score***
gen ser3_4=1 if mc_q8_3_16-mc_q8_4_16==3
replace ser3_4=0 if mc_q8_3_16-mc_q8_4_16!=3
replace ser3_4=.i if mc_q8_4_16==.i
replace ser3_4=.r if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
label variable ser3_4 "Serial 3 Round4 Resta de 3 Ronda4"
label define ser3_4 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser3_4 ser3_4

***SERIAL3 5TH Score***
gen ser3_5=1 if mc_q8_4_16-mc_q8_5_16==3
replace ser3_5=0 if mc_q8_4_16-mc_q8_5_16!=3
replace ser3_5=.i if mc_q8_5_16==.i
replace ser3_5=.r if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
label variable ser3_5 "Serial 3 Round5 Resta de  Ronda5"
label define ser3_5 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser3_5 ser3_5

***SERIAL3 Score***
egen D7_t1_correct=rowtotal(ser3_1-ser3_5)
replace D7_t1_correct=.r if ser3_1==.r & ser3_2==.r & ser3_3==.r & ser3_4==.r & ser3_5==.r
replace D7_t1_correct =.i if ser3_1==.i
label variable D7_t1_correct "Serial 3 correct Resta serial 3 correctos"
label define D7_t1_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D7_t1_correct D7_t1_correct

egen D7_t1_attempts=rownonmiss(ser3_1-ser3_5)
replace D7_t1_attempts=.r if ser3_1==.r & ser3_2==.r & ser3_3==.r & ser3_4==.r & ser3_5==.r
replace D7_t1_attempts =.i if ser3_1==.i
label variable D7_t1_attempts "Serial 3 attempts Resta serial 3 intentos"
label define D7_t1_attempts .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" 
label values D7_t1_attempts D7_t1_attempts

***SERIAL7 1ST Score***
gen ser7_1=1 if inlist(mc_q9_1_16,93,100)
replace ser7_1=0 if mc_q9_1_16!=93 & mc_q9_1_16!=100
replace ser7_1=.i if mc_q9_1_16==.i
replace ser7_1=.s if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
replace ser7_1=.r if mc_q9_1_16==8
label variable ser7_1 "Serial 7 Round1 Resta de 7 Ronda1"
label define ser7_1 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser7_1 ser7_1

***SERIAL7 2ND Score***
gen ser7_2=1 if mc_q9_1_16-mc_q9_2_16==7
replace ser7_2=0 if mc_q9_1_16-mc_q9_2_16!=7
replace ser7_2=.i if mc_q9_2_16==.i
replace ser7_2=.s if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
replace ser7_2=.r if mc_q9_2_16==8
label variable ser7_2 "Serial 7 Round2 Resta de 7 Ronda2"
label define ser7_2 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser7_2 ser7_2

***SERIAL7 3RD Score***
gen ser7_3=1 if mc_q9_2_16-mc_q9_3_16==7
replace ser7_3=0 if mc_q9_2_16-mc_q9_3_16!=7
replace ser7_3=.i if mc_q9_3_16==.i
replace ser7_3=.s if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
replace ser7_3=.r if mc_q9_3_16==8
label variable ser7_3 "Serial 7 Round3 Resta de 7 Ronda3"
label define ser7_3 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser7_3 ser7_3

***SERIAL7 4TH Score***
gen ser7_4=1 if mc_q9_3_16-mc_q9_4_16==7
replace ser7_4=0 if mc_q9_3_16-mc_q9_4_16!=7
replace ser7_4=.i if mc_q9_4_16==.i
replace ser7_4=.s if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
replace ser7_4=.r if mc_q9_4_16==8
label variable ser7_4 "Serial 7 Round4 Resta de 7 Ronda4"
label define ser7_4 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser7_4 ser7_4

***SERIAL7 5TH Score***
gen ser7_5=1 if mc_q9_4_16-mc_q9_5_16==7
replace ser7_5=0 if mc_q9_4_16-mc_q9_5_16!=7
replace ser7_5=.i if mc_q9_5_16==.i
replace ser7_5=.s if mc_q8_1_16==88 & mc_q8_2_16==88 & mc_q8_3_16==88 & mc_q8_4_16==88 & mc_q8_5_16==88
replace ser7_5=.r if mc_q9_5_16==8
label variable ser7_5 "Serial 7 Round5 Resta de 7 Ronda5"
label define ser7_5 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values ser7_5 ser7_5

***SERIAL7 Score***
egen D7_t2_correct= rowtotal(ser7_1-ser7_5)
replace D7_t2_correct=.r if ser7_1==.r & ser7_2==.r & ser7_3==.r & ser7_4==.r & ser7_5==.r
replace D7_t2_correct=.s if ser7_1==.s & ser7_2==.s & ser7_3==.s & ser7_4==.s & ser7_5==.s
replace D7_t2_correct =.i if ser7_1==.i
label variable D7_t2_correct "Serial 7 correct Resta serial 7 correctos"
label define D7_t2_correct .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto" .s "Skip Salto"
label values D7_t2_correct D7_t2_correct

egen D7_t2_attempts= rownonmiss(ser7_1-ser7_5)
replace D7_t2_attempts=.r if ser7_1==.r & ser7_2==.r & ser7_3==.r & ser7_4==.r & ser7_5==.r
replace D7_t2_attempts=.s if ser7_1==.s & ser7_2==.s & ser7_3==.s & ser7_4==.s & ser7_5==.s
replace D7_t2_attempts =.i if ser7_1==.i
label variable D7_t2_attempts "Serial 7 attempts Resta serial 7 intentos"
label define D7_t2_attempts .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  .s "Skip Salto" 
label values D7_t2_attempts D7_t2_attempts

***Verbal Fluency Coded of correct answers***
gen D7_t3_correct=0 if mc_q21_2_16 ==0
replace D7_t3_correct=1 if inrange(mc_q21_2_16,1,8)
replace D7_t3_correct=2 if inrange(mc_q21_2_16,9,18)
replace D7_t3_correct=3 if inrange(mc_q21_2_16,19,24)
replace D7_t3_correct=4 if inrange(mc_q21_2_16,25,36)
replace D7_t3_correct=.r if mc_q21_2_16==99
replace D7_t3_correct=.i if mc_q21_2_16==.i
label variable D7_t3_correct "Verbaly fluency Animals correct Fluidez verbal correctos"
label define D7_t3_correct 1 "1-8" 2 "9-18" 3 "19-24" 4 "25-36" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values D7_t3_correct D7_t3_correct

***Verbal Fluency repeated answers***
gen D7_t3_repeat=mc_q21_3_16
replace D7_t3_repeat=.r if mc_q21_3_16==99
replace D7_t3_repeat=.i if mc_q21_3_16==.i
label variable D7_t3_repeat "Verbaly fluency Repeated Fluidez verbal repetidos"
label define D7_t3_repeat .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"  
label values D7_t3_repeat D7_t3_repeat

***Synbols score of correct answers
gen D7_t4_correct = mc_q36_2_16
replace D7_t4_correct=.r if mc_q36_2_16==99
label variable D7_t4_correct "Symbols and digits correct Simbolos correctos"
label define D7_t4_correct .r ".r: Refused Rehuso" .i".i: Incomplete Incompleto" .c ".c: Short interview Entrevista corta" .l ".l: Physical limitation Limitacion fisica"
label values D7_t4_correct D7_t4_correct

***Similar fruits***
gen similar1 = 1 if mc_q40_1_16==1
replace similar1 = 0 if inlist(mc_q40_1_16,2,8)
replace similar1 = .r if mc_q40_1_16==9
replace similar1 = .c if mc_q40_1_16==.c
replace similar1 = .i if mc_q40_1_16==.i
label variable similar1 "Similar fruits Similitudes frutas"
label define similar1 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values similar1 similar1

***Similar furniture***
gen similar2 = 1 if mc_q41_16==1
replace similar2 = 0 if inlist(mc_q41_16,2,8)
replace similar2 = .r if mc_q41_16==9
replace similar2 = .c if mc_q40_1_16==.c
replace similar2 = .i if mc_q40_1_16==.i
label variable similar2 "Similar furniture Similitudes muebles"
label define similar2 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values similar2 similar2

***Similar flowers***
gen similar3 = 1 if mc_q42_16==1
replace similar3 = 0 if inlist(mc_q42_16,2,8)
replace similar3 = .r if mc_q42_16==9
replace similar3 = .c if mc_q42_16==.c
replace similar3 = .i if mc_q42_16==.i
label variable similar3 "Similar flowers Similitudes flores"
label define similar3 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto"
label values similar3 similar3

****Similarities Score***
egen D7_t5_correct= rowtotal(similar1-similar3)
replace D7_t5_correct=.r if similar1==.r & similar2==.r & similar3==.r
replace D7_t5_correct=.l if similar1==.l
replace D7_t5_correct=.c if similar1==.c
replace D7_t5_correct=.i if similar1==.i
label variable D7_t5_correct "Similarities correct Similitudes correctos"
label define D7_t5_correct .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D7_t5_correct D7_t5_correct

egen D7_t5_attempts= rownonmiss(similar1-similar3)
replace D7_t5_attempts=.r if similar1==.r & similar2==.r & similar3==.r
replace D7_t5_attempts=.l if similar1==.l
replace D7_t5_attempts=.c if similar1==.c
replace D7_t5_attempts=.i if similar1==.i
label variable D7_t5_attempts "Similarities attempts Similitudes intentos"
label define D7_t5_attempts .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D7_t5_attempts D7_t5_attempts


****Go no go attempts***
gen gonogo1 =1 if mc_q43_1_16==1
replace gonogo1=0 if inlist(mc_q43_1_16,0,2)
replace gonogo1=.r if inlist(mc_q43_1_16,8)
replace gonogo1=.l if mc_q10_1_l_16==1
replace gonogo1=.c if mc_q43_1_16==.c
replace gonogo1=.i if mc_q43_1_16==.i
label variable gonogo1 "Gonogo Round1 Aplausos Ronda1"
label define gonogo1 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo1 gonogo1

gen gonogo2 =1 if mc_q43_2_16==1
replace gonogo2=0 if inlist(mc_q43_2_16,0,2)
replace gonogo2=.r if inlist(mc_q43_2_16,8)
replace gonogo2=.l if mc_q10_1_l_16==1
replace gonogo2=.c if mc_q43_2_16==.c
replace gonogo2=.i if mc_q43_2_16==.i
label variable gonogo2 "Gonogo Round2 Aplausos Ronda2"
label define gonogo2 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo2 gonogo2

gen gonogo3 =1 if mc_q43_3_16==0
replace gonogo3=0 if inlist(mc_q43_3_16,1,2)
replace gonogo3=.r if inlist(mc_q43_3_16,8)
replace gonogo3=.l if mc_q10_1_l_16==1
replace gonogo3=.c if mc_q43_3_16==.c
replace gonogo3=.i if mc_q43_3_16==.i
label variable gonogo3 "Gonogo Round3 Aplausos Ronda3"
label define gonogo3 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo3 gonogo3

gen gonogo4 =1 if mc_q43_4_16==1
replace gonogo4=0 if inlist(mc_q43_4_16,0,2)
replace gonogo4=.r if inlist(mc_q43_4_16,8)
replace gonogo4=.l if mc_q10_1_l_16==1
replace gonogo4=.c if mc_q43_4_16==.c
replace gonogo4=.i if mc_q43_4_16==.i
label variable gonogo4 "Gonogo Round4 Aplausos Ronda4"
label define gonogo4 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo4 gonogo4

gen gonogo5 =1 if mc_q43_5_16==0
replace gonogo5=0 if inlist(mc_q43_5_16,1,2)
replace gonogo5=.r if inlist(mc_q43_5_16,8)
replace gonogo5=.l if mc_q10_1_l_16==1
replace gonogo5=.c if mc_q43_5_16==.c
replace gonogo5=.i if mc_q43_5_16==.i
label variable gonogo5 "Gonogo Round5 Aplausos Ronda5"
label define gonogo5 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo5 gonogo5

gen gonogo6 =1 if mc_q43_6_16==0
replace gonogo6=0 if inlist(mc_q43_6_16,1,2)
replace gonogo6=.r if inlist(mc_q43_6_16,8)
replace gonogo6=.l if mc_q10_1_l_16==1
replace gonogo6=.c if mc_q43_6_16==.c
replace gonogo6=.i if mc_q43_6_16==.i
label variable gonogo6 "Gonogo Round6 Aplausos Ronda6"
label define gonogo6 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo6 gonogo6

gen gonogo7 =1 if mc_q43_7_16==0
replace gonogo7=0 if inlist(mc_q43_7_16,1,2)
replace gonogo7=.r if inlist(mc_q43_7_16,8)
replace gonogo7=.l if mc_q10_1_l_16==1
replace gonogo7=.c if mc_q43_7_16==.c
replace gonogo7=.i if mc_q43_7_16==.i
label variable gonogo7 "Gonogo Round7 Aplausos Ronda7"
label define gonogo7 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo7 gonogo7

gen gonogo8 =1 if mc_q43_8_16==1
replace gonogo8=0 if inlist(mc_q43_8_16,0,2)
replace gonogo8=.r if inlist(mc_q43_8_16,8)
replace gonogo8=.l if mc_q10_1_l_16==1
replace gonogo8=.c if mc_q43_8_16==.c
replace gonogo8=.i if mc_q43_8_16==.i
label variable gonogo8 "Gonogo Round8 Aplausos Ronda8"
label define gonogo8 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo8 gonogo8

gen gonogo9 =1 if mc_q43_9_16==1
replace gonogo9=0 if inlist(mc_q43_9_16,0,2)
replace gonogo9=.r if inlist(mc_q43_9_16,8)
replace gonogo9=.l if mc_q10_1_l_16==1
replace gonogo9=.c if mc_q43_9_16==.c
replace gonogo9=.i if mc_q43_9_16==.i
label variable gonogo9 "Gonogo Round9 Aplausos Ronda9"
label define gonogo9 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo9 gonogo9

gen gonogo10 =1 if mc_q43_10_16==0
replace gonogo10=0 if inlist(mc_q43_10_16,1,2)
replace gonogo10=.r if inlist(mc_q43_10_16,8)
replace gonogo10=.l if mc_q10_1_l_16==1
replace gonogo10=.c if mc_q43_10_16==.c
replace gonogo10=.i if mc_q43_10_16==.i
label variable gonogo10 "Gonogo Round10 Aplausos Ronda10"
label define gonogo10 0 "0:  Incorrect Incorrecto" 1 "1:  Correct Correcto" .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values gonogo10 gonogo10

****Go no go Score***
egen D7_t6_correct= rowtotal(gonogo1-gonogo10)
replace D7_t6_correct=.r if gonogo1==.r & gonogo2==.r & gonogo3==.r & gonogo4==.r & gonogo5==.r & gonogo6==.r & gonogo7==.r & gonogo8==.r & gonogo9==.r & gonogo10==.r
replace D7_t6_correct=.l if gonogo1==.l
replace D7_t6_correct=.c if gonogo1==.c
replace D7_t6_correct=.i if gonogo1==.i
label variable D7_t6_correct "Gonogo correct Aplausos correstos"
label define D7_t6_correct .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D7_t6_correct D7_t6_correct

egen D7_t6_attempts= rownonmiss(gonogo1-gonogo10)
replace D7_t6_attempts=.r if gonogo1==.r & gonogo2==.r & gonogo3==.r & gonogo4==.r & gonogo5==.r & gonogo6==.r & gonogo7==.r & gonogo8==.r & gonogo9==.r & gonogo10==.r
replace D7_t6_attempts=.l if gonogo1==.l
replace D7_t6_attempts=.c if gonogo1==.c
replace D7_t6_attempts=.i if gonogo1==.i
label variable D7_t6_attempts "Gonogo attempts Aplausos intentos"
label define D7_t6_attempts .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D7_t6_attempts D7_t6_attempts



				*******************************
				***Executive function domain***
				*******************************
egen D7_ExecutiveFunction_correct=rowtotal(D7_t1_correct D7_t2_correct D7_t3_correct D7_t4_correct D7_t5_correct D7_t6_correct)
replace D7_ExecutiveFunction_correct=.r if D7_t1_correct==.r & D7_t2_correct==.r & D7_t3_correct==.r & D7_t4_correct==.r & D7_t5_correct==.r & D7_t6_correct==.r
replace D7_ExecutiveFunction_correct=.l if D7_t1_correct==.l & D7_t2_correct==.l & D7_t3_correct==.l & D7_t4_correct==.l & D7_t5_correct==.l & D7_t6_correct==.l
replace D7_ExecutiveFunction_correct=.c if D7_t1_correct==.c & D7_t2_correct==.c & D7_t3_correct==.c & D7_t4_correct==.c & D7_t5_correct==.c & D7_t6_correct==.c
replace D7_ExecutiveFunction_correct=.i if D7_t1_correct==.i & D7_t2_correct==.i & D7_t3_correct==.i & D7_t4_correct==.i & D7_t5_correct==.i & D7_t6_correct==.i
label variable D7_ExecutiveFunction_correct "D7: Executive function correct Funcion ejecutiva correctos"
label define D7_ExecutiveFunction_correct .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D7_ExecutiveFunction_correct D7_ExecutiveFunction_correct

***Dummy variable for fluency point and symbol point***
gen fluencypoint=4 if inlist(D7_t3_correct,0,1,2,3,4)
gen symbolspoint=56 if inrange(D7_t6_correct,0,56)

egen D7_ExecutiveFunction_attempts=rowtotal(D7_t1_attempts D7_t2_attempts fluencypoint symbolspoint D7_t2_attempts D7_t6_attempts)			
replace D7_ExecutiveFunction_attempts=.r if D7_t1_attempts==.r & D7_t2_attempts==.r & fluencypoint==.r & symbolspoint==.r & D7_t2_attempts==.r & D7_t6_attempts==.r
replace D7_ExecutiveFunction_attempts=.l if D7_t1_attempts==.l & D7_t2_attempts==.l & fluencypoint==.l & symbolspoint==.l & D7_t2_attempts==.l & D7_t6_attempts==.l
replace D7_ExecutiveFunction_attempts=.c if D7_t1_attempts==.c & D7_t2_attempts==.c & fluencypoint==.c & symbolspoint==.c & D7_t2_attempts==.c & D7_t6_attempts==.c
replace D7_ExecutiveFunction_attempts=.i if D7_t1_correct==.i & D7_t2_correct==.i & D7_t3_correct==.i & D7_t4_correct==.i & D7_t5_correct==.i & D7_t6_correct==.i
label variable D7_ExecutiveFunction_attempts "D7: Executive function attempts Funcion ejecutiva intentos"
label define D7_ExecutiveFunction_attempts .r ".r: Refused Rehuso" .c ".c: Short interview Entrevista corta" .i ".i: Incomplete Incompleto" .l ".l: Physical limitation Limitacion fisica"
label values D7_ExecutiveFunction_attempts D7_ExecutiveFunction_attempts



				
				****************
				***MMSE Score***
				****************
egen MMSE_correct=rowtotal(D1_t1 D1_t2 D1_t3 D1_t4 D1_t5 D1_t6 D1_t8 D1_t9 D2_t1_correct D5_t1_correct D5_t3 D5_t4 D5_t9 D5_t10 D5_t11 D6_t1 D7_t2_correct) 
label variable MMSE_correct "Modified MMSE correct MMSE modificado correcto"

egen MMSE_attempts=rownonmiss(D1_t1 D1_t2 D1_t3 D1_t4 D1_t5 D1_t6 D1_t8 D1_t9 iwr1 iwr2 iwr3 paper1 paper2 paper3 D5_t3 D5_t4 D5_t9 D5_t10 D5_t11 D6_t1 ser7_1 ser7_2 ser7_3 ser7_4 ser7_5) 
label variable MMSE_attempts "Modified MMSE attempts MMSE modificado intentos"




**Delete intermediate vairables for Immediate 3 word recall***
drop iwr1-iwr3
**Delete intermediate vairables for immediate 10 word recall trial 1***
drop wr1_1-wr1_10
**Delete intermediate vairables for immediate 10 word recall trial 2***
drop wr2_1-wr2_10
**Delete intermediate vairables for immediate 10 word recall trial 3***
drop wr3_1-wr3_10
***Drop intermediate variables for short story***
drop shortstory1-shortstory6
***Drop intermediate variables for short story approximate***
drop shortstory1_a-shortstory6_a
***Drop intermediate variables for short story exact***
drop shortstory1_e-shortstory6_e
***Drop intermediate variables for long story **
drop longstory1-longstory25
***Drop intermediate variables for long story aprox **
drop longstory1_a-longstory25_a
***Drop intermediate variables for long story exact**
drop longstory1_e-longstory25_e
***Drop intermediate variables for delayed memory 3 words**
drop dwr1-dwr3
***Drop intermediate variables for 10 word recall***
drop  recall1-recall10
***Drop intermediate variables for short story recall***
drop shortstory1_rec-shortstory6_rec
***Drop intermediate variables for short story approx recall***
drop shortstory_rec_1_a-shortstory_rec_6_a
***Drop intermediate variables for short story exact recall***
drop  shortstory1_rec_e-shortstory6_rec_e
***Drop intermediate variables for long story recall***
drop longstory1_rec-longstory25_rec
****Drop intermediate variables for long story approx recall***
drop longstory1_rec_a-longstory25_rec_a
***Drop intermediate variables for long story exact recall***
drop longstory1_rec_e-longstory25_rec_e
***Drop intermediate variables recognition of 10 words***
drop wordrec1-wordrec20
***Drop intermediate variables for figures and duumy point variables***
drop circle_rec diamond_rec rectangle_rec cube_rec
drop circlepointrec diamondpointrec rectanglepointrec cubepointrec
***Drop intermediate variables for backwards counting**
drop numeracy numeracy_time
***Drop dummy variables for attention domain**
drop numeracypoint visualscanpoint
***Drop intermediate variables for following instructions 3 steps***
drop paper1 paper2 paper3
***Drop intermediate varriables for following instructions 2 steps****
drop floor sky
***Drop intermediate variables for copy figures and dummy variables for points attempted***
drop circle diamond rectangle cube
drop circlepoint diamondpoint rectanglepoint cubepoint
***Drop intermediate variable for point attempted in figure***
drop pentagonspoint
***Drop intermediate variables  for Serial 3***
drop ser3_1 ser3_2 ser3_3 ser3_4 ser3_5
***Drop intermediate variables for Serial 7***
drop ser7_1 ser7_2 ser7_3 ser7_4 ser7_5
***Drop intermediate variables for similarities***
drop similar1 similar2 similar3
***Drop intermediate variables for gor or no go***
drop gonogo1 gonogo2 gonogo3 gonogo4 gonogo5 gonogo6 gonogo7 gonogo8 gonogo9 gonogo10
***Drop intermediate variables for point in Executive function attempts***
drop fluencypoint symbolspoint



