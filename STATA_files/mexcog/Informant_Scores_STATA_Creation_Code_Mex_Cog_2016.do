
***Informant evaluation of subject****


***Cogntiive decline items***

***Change in activities***
gen D1_i1=mi_q1_16
replace D1_i1=.d if mi_q1_16==8
label variable D1_i1 "D1_i1: Change in daily activities. Cambio en sus actividades diarias."
label define D1_i1 .d ".d:Does not know No sabe"
label values D1_i1 D1_i1

***Decline in mental functions***
gen D1_i2= mi_q2_16
label variable D1_i2 "D1_i2: Decline in mental functioning Disminución en habilidad mental."


***Difficulty rememeberting things***
gen D1_i3= mi_q3_16 
replace D1_i3=.d if mi_q3_16==8
replace D1_i3=.r if mi_q3_16==9
label variable D1_i3 "D1_i3: Difficulties remembering things Dificultades para recordar cosas"
label define D1_i3 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso"
label values D1_i3 D1_i3

***Forgets where puts things***
gen D1_i4=mi_q4_16
replace D1_i4=.d if mi_q4_16==8
label variable D1_i4 "D1_i4: Forgets where (he/she) has put things Olvida donde puso las cosas"
label define D1_i4 .d ".d:Does not know No sabe"
label values D1_i4 D1_i4

***Forgets where keeps things***
gen D1_i5=mi_q5_16
replace D1_i5=.d if mi_q5_16==8
replace D1_i5=.i if mi_q5_16==.i
label variable D1_i5 "D1_i5: Forgets where things are usually kept Olvida dónde se guardan las cosas"
label define D1_i5 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i5 D1_i5

***Forgets friends name***
gen D1_i6=mi_q6_16
replace D1_i6=.r if mi_q6_16==9
replace D1_i6=.d if mi_q6_16==8
replace D1_i6=.i if mi_q6_16==.i
label variable D1_i6 "D1_i6: Does (he/she) forget the names of friends Olvida el nombre de sus amigos"
label define D1_i6 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i6 D1_i6
   
***Forgets family members name***   
gen D1_i7= mi_q7_16
replace D1_i7=.d if mi_q7_16==8
replace D1_i7=.i if mi_q7_16==.i
label variable D1_i7 "D1_i7: Forgets the names of family members Olvida nombre de miembros de la familia"
label define D1_i7 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i7 D1_i7

***Forgets topic in middle of conversation***   
gen D1_i8=mi_q8_16
replace D1_i8=.d if mi_q8_16==8
replace D1_i8=.i if mi_q8_16==.i
label variable D1_i8 "D1_i8: Forgets in the middle of a conversation Olvida en la mitad de una conversacion"
label define D1_i8 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i8 D1_i8

   
***Difficulty finding correct words***   
gen D1_i9= mi_q9_16
replace D1_i9=.d if mi_q9_16==8
replace D1_i9=.r if mi_q9_16==9
replace D1_i9=.i if mi_q9_16==.i
label variable D1_i9 "D1_i9: Difficulty finding the right words Dificultad para encontrar la palabra correcta."
label define D1_i9 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i9 D1_i9
   
***Uses wrong words***   
gen D1_i10=mi_q10_16
replace D1_i10=.d if mi_q10_16==8
replace D1_i10=.r if mi_q10_16==9
replace D1_i10=.i if mi_q10_16==.i
label variable D1_i10 "D1_i10: Does (he/she) use wrong words Utiliza palabras erróneas /equivocadas"
label define D1_i10 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i10 D1_i10

***Speaks about things that happened in the past**   
gen D1_i11=mi_q11_16
replace D1_i11=.d if mi_q11_16==8
replace D1_i11=.r if mi_q11_16==9
replace D1_i11=.i if mi_q11_16==.i
label variable D1_i11 "D1_i11: Tends to talk about long ago Habla de cosas que ocurrieron en el pasado"
label define D1_i11 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i11 D1_i11
   
***Forgets last time seen you**   
gen D1_i12=mi_q12_16
replace D1_i12=.d if mi_q12_16==8
replace D1_i12=.r if mi_q12_16==9
replace D1_i12=.i if mi_q12_16==.i
label variable D1_i12 "D1_i12: Forgets when last saw you Se le olvida la última vez que lo vio a Usted"
label define D1_i12 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i12 D1_i12
   
***Forgets what they did the day before**   
gen D1_i13=mi_q13_16
replace D1_i13=.d if mi_q13_16==8
replace D1_i13=.r if mi_q13_16==9
replace D1_i13=.i if mi_q13_16==.i
label variable D1_i13 "D1_i13: Forgets what happened the day before Se le olvida qué hizo el día anterior"
label define D1_i13 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i13 D1_i13
   
***Forgets where they are**   
gen D1_i14=mi_q14_16
replace D1_i14=.d if mi_q14_16==8
replace D1_i14=.r if mi_q14_16==9
replace D1_i14=.i if mi_q14_16==.i 
label variable D1_i14 "D1_i14:  Does (he/she) forget where (he/she) is Se le olvida dónde está"
label define D1_i14 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i14 D1_i14
   
***Gets lost in the neighborhood**   
gen D1_i15=mi_q15_16
replace D1_i15=0 if mi_q15_16==3
replace D1_i15=.d if mi_q15_16==8
replace D1_i15=.r if mi_q15_16==9
replace D1_i15=.i if mi_q15_16==.i
label variable D1_i15 "D1_i15: Gets lost in the community Se pierde donde vive"
label define D1_i15 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i15 D1_i15
   
***Gets lost in the house**   
gen D1_i16=mi_q16_16
replace D1_i16=.d if mi_q16_16==8
replace D1_i16=.r if mi_q16_16==9
replace D1_i16=.i if mi_q16_16==.i
label variable D1_i16 "D1_i16: Gets lost in own home Se pierde en la casa"
label define D1_i16 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i16 D1_i16

***Has difficulty in adjusting to change in daily routine**   
gen D1_i17=mi_q20_16
replace D1_i17=0 if mi_q20_16==3
replace D1_i17=.r if mi_q20_16==9
replace D1_i17=.i if mi_q20_16==.i
label variable D1_i17 "D1_i17: Difficulty adapting to changes in routine Dificultad adaptarse a cambios."
label define D1_i17 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i17 D1_i17

***Change in ability to reason**   
gen D1_i18=0 if mi_q21_16==0
replace D1_i18=0 if mi_q21_16==1
replace D1_i18=1 if mi_q21_16==2
replace D1_i18=.r if mi_q21_16==9
replace D1_i18=.i if mi_q21_16==.i
label variable D1_i18 "D1_i18: Change in ability to think and reason Cambio en su habilidad para pensar/razonar"
label define D1_i18 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i18 D1_i18

***Mistaken family**   
gen D1_i19=mi_q25_16
replace D1_i19=.d if mi_q25_16==8
replace D1_i19=.r if mi_q25_16==9
replace D1_i19=.i if mi_q25_16==.i
label variable D1_i19 "D1_i19: Ever confused with another person Confundido con otra persona"
label define D1_i19 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i19 D1_i19

***Difficluty making decisions**   
gen D1_i20=mi_q26_16
replace D1_i20=.d if mi_q26_16==8
replace D1_i20=.r if mi_q26_16==9
replace D1_i20=.i if mi_q26_16==.i
label variable D1_i20 "D1_i20: Difficulty taking everyday decisions Dificultad en tomar decisiones cotidianas"
label define D1_i20 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i20 D1_i20


***Reasoning confusing or ilogical**   
gen D1_i21=mi_q27_16
replace D1_i21=.d if mi_q27_16==8
replace D1_i21=.r if mi_q27_16==9
replace D1_i21=.i if mi_q27_16==.i
label variable D1_i21 "D1_i21: His reasoning is confusing or illogical Pensamiento confuso o ilógico"
label define D1_i21 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D1_i21 D1_i21


				
***Cognitive decline score***

egen CognitiveDec_correct=rowtotal(D1_i1-D1_i21)
label variable CognitiveDec_correct "Cognitive decline Deteriorio cognitivo"


*Attempted cognitive decline acore intermediate variables***

gen vardummy1  = 3 if inlist(D1_i1, 0,1,2,3)
gen vardummy2 = 1 if inlist(D1_i2, 0,1)
gen vardummy3 = 1 if inlist(D1_i3, 0,1)
gen vardummy4 = 2 if inlist(D1_i4, 0,1,2)
gen vardummy5 = 2 if inlist(D1_i5, 0,1,2)
gen vardummy6 = 2 if inlist(D1_i6, 0,1,2)
gen vardummy7 = 2 if inlist(D1_i7, 0,1,2)
gen vardummy8 = 2 if inlist(D1_i8, 0,1,2)
gen vardummy9 = 2 if inlist(D1_i9, 0,1,2)
gen vardummy10 = 2 if inlist(D1_i10, 0,1,2)
gen vardummy11 = 2 if inlist(D1_i11, 0,1,2)
gen vardummy12 = 2 if inlist(D1_i12, 0,1,2)
gen vardummy13 = 2 if inlist(D1_i13, 0,1,2)
gen vardummy14 = 2 if inlist(D1_i14, 0,1,2)
gen vardummy15 = 2 if inlist(D1_i15, 0,1,2)
gen vardummy16 = 2 if inlist(D1_i16, 0,1,2)
gen vardummy17 = 2 if inlist(D1_i17, 0,1,2)
gen vardummy18 = 1 if inlist(D1_i18, 0,1)
gen vardummy19 = 2 if inlist(D1_i19, 0,1,2)
gen vardummy20 = 2 if inlist(D1_i20, 0,1,2)
gen vardummy21 = 1 if inlist(D1_i21, 0,1)


egen CognitiveDec_attempts=rowtotal(vardummy1 - vardummy21)
label variable CognitiveDec_attempts "Cognitive decline attempts Deteriorio cognitivo intentos"


drop vardummy1-vardummy21


***Functional decline items***

***Has difficulty realizing daily activities**   
gen D2_i1=mi_q17_16
replace D2_i1=.d if mi_q17_16==8
replace D2_i1=.r if mi_q17_16==9
replace D2_i1=.i if mi_q17_16==.i
label variable D2_i1 "D2_i1: Difficulty performing chores Dificultades para realizar tareas en casa"
label define D2_i1 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_i1 D2_i1

***Stopped doing activities or hobbies**   
gen D2_i2=0 if mi_q18_16==0
replace D2_i2=1 if mi_q18_16==1
replace D2_i2=.d if mi_q18_16==8
replace D2_i2=.r if mi_q18_16==9
replace D2_i2=.i if mi_q18_16==.i
label variable D2_i2 "D2_i2: Stopped doing activities or hobbies Ha dejado de realizar alguna actividad"
label define D2_i2 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_i2 D2_i2

***Change in ability to handle money**   
gen D2_i3=mi_q19_16
replace D2_i3=.r if mi_q19_16==9
replace D2_i3=.i if mi_q19_16==.i
label variable D2_i3 "D2_i3: Change in ability to handle money Cambio en  habilidad para utilizar el dinero"
label define D2_i3 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_i3 D2_i3

***Ability to eat**   
gen D2_i4=0 if mi_q22_16==0
replace D2_i4=1 if mi_q22_16==1
replace D2_i4=1 if mi_q22_16==2
replace D2_i4=1 if mi_q22_16==3
replace D2_i4=.r if mi_q22_16==9
replace D2_i4=.i if mi_q22_16==.i
label variable D2_i4 "D2_i4: Regarding eating, would you say Sobre la habilidad para comer"
label define D2_i4 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_i4 D2_i4

***Ability to dress**   
gen D2_i5=mi_q23_16
replace D2_i5=.r if mi_q23_16==9
replace D2_i5=.i if mi_q23_16==.i
label variable D2_i5 "D2_i5: Regarding dressing Para vestirse"
label define D2_i5 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_i5 D2_i5

***Difficulty holding urine**   
gen D2_i6=mi_q24_1_16
replace D2_i6=.r if mi_q24_1_16==9
replace D2_i6=.i if mi_q24_1_16==.i
label variable D2_i6 "D2_i6: Difficulty holding or controlling urine Dificultad en controlar orina"
label define D2_i6 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_i6 D2_i6

***Difficulty holding bowel**   
gen D2_i7=mi_q24_2_16
replace D2_i7=.r if mi_q24_2_16==9
replace D2_i7=.i if mi_q24_2_16==.i
label variable D2_i7 "D2_i7: Difficulty controlling bowel movements Dificultad controlar la evacuación?"
label define D2_i7 .d ".d:Does not know No sabe" .r ".r: Refused Rehuso" .i ".i: Incomplete Incompleto"
label values D2_i7 D2_i7




***Functional decline score***

egen FunctionalDec_correct=rowtotal(D2_i1-D2_i7)
label variable FunctionalDec_correct "Functional decline Deteriorio funcional"


*Attempted ducntional decline score intermediate variables***

gen vardummy22 = 3 if inlist(D2_i1, 0,1,2,3)
gen vardummy23 = 3 if inlist(D2_i2, 0,1,2,3)
gen vardummy24 = 2 if inlist(D2_i3, 0,1,2)
gen vardummy25 = 2 if inlist(D2_i4, 0,1,2)
gen vardummy26 = 1 if inlist(D2_i5, 0,1)
gen vardummy27 = 1 if inlist(D2_i6, 0,1)
gen vardummy28 = 1 if inlist(D2_i7, 0,1)

egen FunctionalDec_attempts=rowtotal(vardummy22-vardummy28)
label variable FunctionalDec_attempts "Functional decline attempts Deteriorio funcional intentos"

drop vardummy22-vardummy28


				**************************
				***CSID Score***
				**************************

***TOTAL CSID Score and attempts***

***TOTAL CSID Score and attempts***
egen CSID_correct=rowtotal(CognitiveDec_correct  FunctionalDec_correct)
label variable CSID_correct "CSID score"


egen CSID_attempts=rowtotal(CognitiveDec_attempts FunctionalDec_attempts)
label variable CSID_attempts "CSID score attempts"



******************************************************************
***********Filer Variables for long and Short intrview************
******************************************************************
gen dumfil3=1 if inlist(mi_q4_16,1,2)
gen dumfil1=1 if inlist(mi_q2_16,1)
gen dumfil4=1 if inlist(mi_q13_16,1,2)
gen dumfil5=1 if inlist(mi_q14_16,1,2)
gen dumfil6=1 if inlist(mi_q21_16,2)
gen dumfil7=1 if inlist(mi_q23_16,1,2,3)
gen dumfil2=1 if inlist(mi_q3_16,1)

egen sumfil1= rowtotal(dumfil3-dumfil4)
egen sumfil2= rowtotal(dumfil1-dumfil2)

gen filtro1=1 if  sumfil1 >= 2
replace filtro1=0 if  sumfil1 < 2

gen filtro2=1 if  sumfil2 >= 2
replace filtro2=0 if  sumfil2 < 2
